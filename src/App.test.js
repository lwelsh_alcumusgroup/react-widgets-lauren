import React from 'react';
import App from './App';
import {render, fireEvent, waitForElement, flushEffects} from 'react-testing-library';
import {globalWidgets} from './component';

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

jest.mock('popper.js', () => {
    const PopperJS = jest.requireActual('popper.js');

    return class {
        static placements = PopperJS.placements;

        constructor() {
            return {
                destroy: () => {
                },
                scheduleUpdate: () => {
                }
            };
        }
    };
});

let setItem = jest.spyOn(window.localStorage.__proto__, 'setItem');
beforeEach(()=>{
    globalWidgets.removeAllListeners();
    flushEffects();
});

it('Basic smoke test, a bit of clicking around and ensuring things update', async () => {
    const {getByTestId,getByText, queryByText, asFragment} = render(<App/>);
    expect(getByText('Wait...')).toBeInTheDocument();
    await waitForElement(()=>getByText('Dashboard'));
    expect(getByText('Incidents')).toBeInTheDocument();
    expect(queryByText('Marital Status')).toEqual(null);
    fireEvent.click(getByText('Questions'));
    expect(getByText('Marital Status')).toBeInTheDocument();
    expect(queryByText('Title')).toEqual(null);
    fireEvent.click(getByTestId('enteredit'));
    expect(queryByText('Press Me')).toBe(null);
    flushEffects();
    fireEvent.click(getByTestId('text-email'));
    expect(queryByText('Title')).not.toEqual(null);
    expect(queryByText('Personal Details')).not.toEqual(null);
    let title = getByTestId('titleinput');
    fireEvent.change(title, {target: {value: 'Hello World'}});
    fireEvent.click(getByTestId('exitedit'));
    expect(queryByText('Hello World')).not.toEqual(null);
    fireEvent.click(getByTestId('enteredit'));
    flushEffects();
    fireEvent.click(getByTestId('text-email'));
    setItem.mockReset();
    expect(getByTestId('editQuestion')).toBeInTheDocument();
    fireEvent.change(getByTestId('text-email'), {target: {value: 'Mike'}});
    expect(setItem).toHaveBeenCalledTimes(2); //Once for the design and once for the document
    fireEvent.click(getByTestId('list-gender'));
    fireEvent.click(getByTestId('contextopen'));
    expect(queryByText('Press Me')).not.toBe(null);
    expect(asFragment()).toMatchSnapshot();
});

it('should pass a dashboard test', async function() {
    const {getByTestId, getByText, queryByText, asFragment} = render(<App/>);
    await waitForElement(() => getByText('Dashboard'));
    expect(getByText('Incidents')).toBeInTheDocument();
    fireEvent.click(getByTestId('enteredit'));
    flushEffects();
    fireEvent.click(getByText('Incidents'));
    expect(asFragment()).toMatchSnapshot();
});
