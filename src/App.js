import React, {useState} from 'react';
import useAsync from './component/use-async';
import Widgets from './component/widgets';
import dashboardExample from './examples/example/dashboard.json';
import questionsExample from './examples/example/questions.json';
import components from './examples/components';
import injectSheet from 'react-jss';
import {Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import {globalWidgets} from './component/events';
import {EditorFrame} from './component/editor';
import {JssProvider} from 'react-jss';

let document = JSON.parse(localStorage.getItem('document') || JSON.stringify({}));
let design = JSON.parse(localStorage.getItem('design') || JSON.stringify(questionsExample));

let dashboardDocument = JSON.parse(localStorage.getItem('dashboardDocument') || JSON.stringify({}));
let dashboardDesign = JSON.parse(localStorage.getItem('dashboardDesign') || JSON.stringify(dashboardExample));

let bag = {};

const styles = {
    body: {
        flexGrow: 1,
        overflowY: 'auto',
    },
    nav: {
        padding: '0.4em',
        '& .nav-item': {
            cursor: 'pointer',
        },
    },
    frame: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },
};

function Application({classes}) {
    let hasComponents = useAsync(async () => {
        try {
            await Promise.all(components.map(async item => {
                let module = await import(`./examples/${item}`);
                module.default(globalWidgets);
            }));
        } catch(e) {
            console.error(e.stack);
        }
        return true;
    });
    let [mode, setMode] = useState('dashboard');
    let [editable, setEditable] = useState(true);
    let [readonly, setReadOnly] = useState(document.readonly);
    return (
        !hasComponents ? <div>Wait...</div> : <JssProvider><div className={classes.frame}>
            <Nav pills className={classes.nav}>
                <NavItem>
                    <NavLink active={mode === 'questions'} onClick={() => setMode('questions')}>Questions</NavLink>
                </NavItem>
                <NavItem className="mr-auto">
                    <NavLink active={mode === 'dashboard'} onClick={() => setMode('dashboard')}>Dashboard</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink active={editable} onClick={() => setEditable(!editable)}>Editable</NavLink>
                </NavItem>

            </Nav>
            <TabContent activeTab={mode} className={classes.frame}>
                <TabPane tabId="questions" className={classes.frame}>
                    {mode === 'questions' && <Widgets id="questions" bag={bag} readOnly={readonly} setReadOnly={setReadOnly} editable={editable}
                        type="survey"
                        className={classes.body} document={document} design={design} onUpdated={() => {

                            localStorage.setItem('design', JSON.stringify(design));
                            localStorage.setItem('document', JSON.stringify(document));
                        }}>
                        <EditorFrame/>
                    </Widgets>}
                </TabPane>
                <TabPane tabId="dashboard" className={classes.frame}>
                    {mode === 'dashboard' && <Widgets id="dashboards" editable={editable}
                        type="dashboard" document={dashboardDocument} design={dashboardDesign} onUpdated={() => {
                            localStorage.setItem('dashboardDesign', JSON.stringify(dashboardDesign));
                            localStorage.setItem('dashboardDocument', JSON.stringify(dashboardDocument));
                        }}>
                        <EditorFrame/>
                    </Widgets>}
                </TabPane>
            </TabContent>

        </div>
        </JssProvider>
    );
}

export default injectSheet(styles)(Application);
