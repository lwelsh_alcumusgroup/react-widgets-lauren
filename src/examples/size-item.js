import injectSheet from 'react-jss';
import {globalWidgets} from '../component';
import React from 'react';
import {Col, FormGroup, Input, Label} from 'reactstrap';
import {useFocused} from '../component/contexts';

const styles = {};


const SizeInfo = injectSheet(styles)(function SizeInfo() {
    const [, updateList] = useFocused('selectedWidgets');
    const [item, update] = useFocused('selectedWidget');
    return (
        <>
            <FormGroup row>
                <Label sm={2}>Width</Label>
                <Col sm={10}>
                    <Input type="range" min={2} max={12} value={item.width || 3} onChange={e => {
                        update({width: e.target.value});
                        updateList();
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Height</Label>
                <Col sm={10}>
                    <Input type="range" min={3} max={10} value={item.height || 1} onChange={e => {
                        update({height: e.target.value});
                        updateList();
                    }}/>
                </Col>
            </FormGroup>
        </>
    );
});

export default function (widgets=globalWidgets) {
    widgets.editor('dashboard', function ({editor, focus: {selectedWidget}}) {
        if (selectedWidget) {
            editor.tabs.size = {title: 'Size', content: [SizeInfo]};
        }
    });
}
