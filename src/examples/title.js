import React from 'react';
import {Col, Form, FormGroup, Input, Label} from 'reactstrap';
import {heading} from './styles';
import injectSheet from 'react-jss';
import {useDesign} from '../component/contexts';
import {useLayout, useTabs} from '../component';

const styles = {
    title: {
        fontWeight: 'bold'
    },
    heading
};
const RenderTab = injectSheet(styles)(({design, classes}) => {
    let update;
    ([design, update] = useDesign(design));
    return <Form>
        <h4 className={classes.heading}>Document</h4>
        <FormGroup row>
            <Label sm={2} for="title">Title</Label>
            <Col sm={10}>
                <Input type="text" id="title" data-testid="titleinput" onFocus={e => e.target.select()} value={design.title}
                    onChange={(event) => {
                        update({title: event.target.value});
                    }}/>
            </Col>
        </FormGroup>
    </Form>;
});
RenderTab.priority = 0;


const RenderTitle = injectSheet(styles)(function ({classes}) {
    const [design] = useDesign();
    return <Col>
        <div className={classes.title}>{design.title}</div>
    </Col>;
});
RenderTitle.priority = 2;

export default function(widgets) {
    widgets.configure('*', function () {
        const [design] = useDesign();
        design.title && useLayout({headerCentre: [RenderTitle]});
    });
    widgets.editor('*', function() {
        const [design] = useDesign();
        design.title !== undefined && useTabs(RenderTab);
    });
}


