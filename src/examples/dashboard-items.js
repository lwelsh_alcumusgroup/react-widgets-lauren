/* This is pretty much the whole logic for the dashboard*/


import React, {useState} from 'react';
import {
    arrayMoveInPlace,
    globalWidgets,
    inPriorityOrder,
    SortableWidget,
    SortableWidgetContainer,
    useEditMode,
    useEvents, useInline,
    useLayout,
    useTabs,
    useWidgetContext,
} from '../component';

import {ButtonDropdown, Col, DropdownItem, DropdownMenu, DropdownToggle, FormGroup, Input, Label, Row} from 'reactstrap';
import {generate} from 'shortid';
import {heading} from './styles';
import injectSheet from 'react-jss';
import classnames from 'classnames';
import {FiPlus} from 'react-icons/fi';
import {useDesign, useFocused} from '../component/contexts';
import {ColorPicker} from './color-picker';
import Switch from '@trendmicro/react-toggle-switch';
import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css';

const styles = {
    heading,
    questionRow: {
        margin: {
            top: 5,
        },
    },
    tools: {
        padding: 0,
        margin: 0,
        zIndex: 2,
        position: 'absolute',
        right: 0,
        bottom: 19,
    },
    dashRow: {
        marginTop: 16,
    },
};


const DashboardItems = injectSheet(styles)(function DashboardItems({classes, dashboardItems}) {
    const [items, update] = useDesign(dashboardItems);
    const dashboardContext = {dashboardItems: items};
    const [editMode] = useEditMode();
    const {fallbackCaption} = useWidgetContext();
    const widgets = useEvents();
    widgets.emit('filter.dashboardItems', dashboardContext);
    return <SortableWidgetContainer axis="xy"
        distance={4}
        isSelected={(focus) => focus.selectedWidgets === items}
        focusOn={{selectedWidgets: items}}
        design={items}
        onSortEnd={({oldIndex, newIndex}) => {
            arrayMoveInPlace(items, oldIndex, newIndex);
            update();
        }}
        type="dashboardItems">
        {
            dashboardContext.dashboardItems.length
                ? <Row className={classes.dashRow}> {dashboardContext.dashboardItems.map((item, index) => {
                    return (

                        <SortableWidget
                            key={item.id}
                            index={index}
                            disabled={!editMode}
                            design={item}
                            isSelected={focus => focus.selectedWidget === item}
                            focusOn={{
                                selectedWidgets: items,
                                selectedWidget: item,
                            }}
                            endOfLineClass={classes.tools}
                            className={`col-${item.colSize || 'md'}-${item.width || 3}`}
                            type={[`dashboardItem.${item.type || 'default'}`,
                                'list', 'copyable',
                            ]}/>
                    );
                })}
                </Row>
                : (
                    <div>
                        {fallbackCaption !== undefined ? fallbackCaption : 'No widgets'}
                    </div>)
        }
    </SortableWidgetContainer>;
});

function Render() {
    const [design] = useDesign();
    return <Col>
        <DashboardItems dashboardItems={design.dashboardItems}/>
    </Col>;
}


export {DashboardItems};


function createDefaultItem(props) {
    return Object.assign({
        id: generate(),
        title: '',
        description: '',
    }, props);
}

export {createDefaultItem};


const AddDashboardItem = injectSheet(styles)(({classes, types}) => {
    const [open, setOpen] = useState(false);
    const [design, update] = useDesign();
    return (
        <Row className={classnames(classes.questionRow, 'no-gutters')}>
            <Col>&nbsp;</Col>
            <Col xs="auto">
                <ButtonDropdown direction="left" size="sm" isOpen={open} toggle={() => setOpen(!open)}>
                    <DropdownToggle color="success">
                        <FiPlus/> Widget
                    </DropdownToggle>
                    <DropdownMenu>
                        {types.sort(inPriorityOrder).map((type, index) => (
                            <DropdownItem onClick={() => {
                                design.push(type.create());
                                update();
                            }} key={index}>
                                {type.name}
                            </DropdownItem>
                        ))}
                    </DropdownMenu>
                </ButtonDropdown>
            </Col>
        </Row>
    );
});


const DashboardInfo = injectSheet(styles)(function DashboardInfo({classes}) {
    const [selectedWidget, update] = useFocused('selectedWidget');
    return (
        <>
            <h4 className={classes.heading}>Dashboard Widget</h4>
            <FormGroup row>
                <Label sm={2}>Title</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.title} onFocus={e => e.target.select()}
                        onChange={event => {
                            update({title: event.target.value});
                        }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Description</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.description} onFocus={e => e.target.select()}
                        onChange={(event) => {
                            update({description: event.target.value});
                        }}/>
                </Col>
            </FormGroup>
            <h4 className={classes.heading}>Chart</h4>
            <FormGroup row>
                <Label sm={2}>Background</Label>
                <Col sm={10}>
                    <ColorPicker value={selectedWidget.color || '#ffffff'} onChange={color => {
                        update({color: color.hex});
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Foreground</Label>
                <Col sm={10}>
                    <ColorPicker value={selectedWidget.axisColor || '#222'} onChange={color => {
                        update({axisColor: color.hex});
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Adjust</Label>
                <Col sm={10}>
                    <ColorPicker value={selectedWidget.adjustColor || '#000'} onChange={color => {
                        update({adjustColor: color.hex});
                    }}/>
                </Col>
            </FormGroup>
        </>
    );
});
DashboardInfo.priority = 2;

const StretchInfo = injectSheet(styles)(function StretchInfo({classes}) {
    const [design, update] = useDesign();
    return <>
        <h4 key={1} className={classes.heading}>Cards</h4>
        <FormGroup key={2} row>
            <Label sm={2}>Expand Cards</Label>
            <Col sm={10}>
                <Switch type="checkbox" checked={design.fillHeight}
                    onChange={() => {
                        update({fillHeight: !design.fillHeight});
                    }}/>
            </Col>
        </FormGroup>
    </>;
});
StretchInfo.priority = 10;

export default function (widgets = globalWidgets) {
    widgets.editor('dashboard', function () {
        const [selectedWidget] = useFocused('selectedWidget');
        const [, addTab] = useTabs(StretchInfo);
        selectedWidget && addTab(DashboardInfo);
    });
    widgets.render('dashboardItems', function () {
        const widgets = useEvents();
        let types = [];
        widgets.emit('dashboardItems.types', types);
        useInline(props => <AddDashboardItem types={types} {...props}/>);
    });
    widgets.configure('dashboard', function () {
        const [design] = useDesign();
        if (design.dashboardItems) {
            useLayout({content: [Render]});
        }
    });

}
