import React from 'react';
import TestWidget from './filter-panel';
import {events} from '../../test/event-payloads';
import {render} from 'react-testing-library';
import {Widgets} from '../component';
import {widgetEventsFactory} from '../component';

describe('FilterPanel', function () {
    let payload;
    let widgets;

    beforeAll(() => {
        payload = {...events.configure.survey};
        widgets = widgetEventsFactory();
        TestWidget(widgets);
    });

    it('Should match snapshot with given payload on configure.survey event', () => {
        function Test() {
            return <Widgets widgets={widgets} {...payload} document={{}}/>;
        }
        const {asFragment, getByText}  = render(<Test/>);
        expect(getByText('Filter'));
        expect(asFragment()).toMatchSnapshot();
    });
});
