import {Button} from 'reactstrap';
import {FiChevronUp, FiChevronDown} from 'react-icons/fi';
import React from 'react';
import {WhenSelected} from '../component/when-selected';
import {useDesign, useParent} from '../component/contexts';
import {useEndOfLine} from '../component';

const ListMove = () => {
    let [item] = useDesign();
    let [list, update] = useParent();
    let index = list.indexOf(item);
    return (
        <WhenSelected>
            <Button disabled={index === 0} onClick={() => {
                list.splice(index, 1);
                list.splice(index-1, 0, item);
                update();
            }} color="primary">
                <FiChevronUp/>
            </Button>
            <Button disabled={index === list.length-1} onClick={() => {
                list.splice(index, 1);
                list.splice(index + 1, 0, item);
                update();
            }} color="primary">
                <FiChevronDown/>
            </Button>
        </WhenSelected>
    );
};
ListMove.priority = 50;

export default function(widgets) {
    widgets.render('list', function () {
        useEndOfLine(ListMove);
    });
}
