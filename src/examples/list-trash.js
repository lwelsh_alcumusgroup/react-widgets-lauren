import {Button} from 'reactstrap';
import {FiTrash} from 'react-icons/fi';
import React from 'react';
import {WhenSelected} from '../component/when-selected';
import {useParent, useDesign} from '../component/contexts';
import {useEndOfLine} from '../component';

const Render = function() {
    const [item] = useDesign();
    const [list, update] = useParent();
    return <WhenSelected>
        <Button onClick={() => {
            list.splice(list.findIndex(i => i === item), 1);
            update();
        }} color="danger">
            <FiTrash/>
        </Button>
    </WhenSelected>;
};
Render.priority = 100;


export default function (widgets) {
    widgets.render('list', function () {
        useEndOfLine(Render);
    });
}
