import {generate} from 'shortid';
import {Button} from 'reactstrap';
import {FiCopy} from 'react-icons/fi';
import React from 'react';
import {WhenSelected} from '../component/when-selected';
import {useParent, useDesign} from '../component/contexts';
import {useEndOfLine} from '../component';

const Render = function() {
    const [list, update] = useParent();
    const [item] = useDesign();
    return <WhenSelected>
        <Button onClick={() => {
            for (let i = 0; i < 1; i++) {
                let copy = JSON.parse(JSON.stringify(item));
                copy.id = generate();
                list.splice(list.findIndex(i => i === item) + 1, 0, copy);
            }
            update();

        }} color="success">
            <FiCopy/>
        </Button>
    </WhenSelected>;

};
Render.priority = -1;

export default function (widgets) {
    widgets.render('copyable', function () {
        useEndOfLine(Render);
    });
}
