import {inPriorityOrder} from '../component/index';
import {ButtonDropdown, Col, DropdownItem, DropdownMenu, DropdownToggle, FormGroup, Input, Label, Row} from 'reactstrap';
import {FiPlus} from 'react-icons/fi';
import React, {useState} from 'react';
import {generate} from 'shortid';
import injectSheet from 'react-jss';
import classnames from 'classnames';
import {heading} from './styles';
import {useFocused} from '../component/contexts';
import {useDesign, useEvents, useInline, useTabs} from '../component';

function createDefaultQuestion(props) {
    return Object.assign({
        id: generate(),
        name: '',
        question: '',
    }, props);
}

export {createDefaultQuestion};

const styles = {
    questionRow: {
        margin: {
            top: 5,
        },
    },
    heading,
};

const AddQuestion = injectSheet(styles)(({classes, types}) => {
    const [open, setOpen] = useState(false);
    const [design, update] = useDesign();
    return (
        <Row className={classnames(classes.questionRow, 'no-gutters')}>
            <Col>&nbsp;</Col>
            <Col xs="auto">
                <ButtonDropdown direction="left" size="sm" isOpen={open} toggle={() => setOpen(!open)}>
                    <DropdownToggle color="success">
                        <FiPlus/> Question
                    </DropdownToggle>
                    <DropdownMenu>
                        {types.sort(inPriorityOrder).map((type, index) => (
                            <DropdownItem onClick={() => {
                                design.push(type.create());
                                update();
                            }} key={index}>
                                {type.name}
                            </DropdownItem>
                        ))}
                    </DropdownMenu>
                </ButtonDropdown>
            </Col>
        </Row>
    );
});


const QuestionInfo = injectSheet(styles)(function QuestionInfo({classes}) {
    let [question, update] = useFocused('selectedQuestion');
    if (!question) return <></>;
    return (
        <>
            <h4 className={classes.heading}>Question</h4>
            <FormGroup row>
                <Label sm={2}>Name</Label>
                <Col sm={10}>
                    <Input data-testid="editQuestionName" type="text" value={question.name} onFocus={e => e.target.select()} onChange={event => {
                        update({name: event.target.value});
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Question</Label>
                <Col sm={10}>
                    <Input data-testid="editQuestion" type="text" value={question.question} onFocus={e => e.target.select()} onChange={(event) => {
                        update({question: event.target.value});
                    }}/>
                </Col>
            </FormGroup>
        </>
    );
});

export default function (widgets) {
    widgets.render('questions', function () {
        let widgets = useEvents();
        let types = [];
        widgets.emit('questions.types', types);
        useInline(props => <AddQuestion types={types} {...props}/>);
    });
    widgets.editor('*', function () {
        useTabs(QuestionInfo);
    });
}
