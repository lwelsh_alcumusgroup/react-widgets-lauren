import React, {useState} from 'react';
import shortid from 'shortid';
import {Button, Popover} from 'reactstrap';
import {SketchPicker} from 'react-color';

function ColorPicker({value, onChange, ...props}) {
    const [id] = useState(shortid.generate());
    const [open, setOpen] = useState(false);
    return <>
        <Button {...props} onClick={() => setOpen(true)} id={`button${id}`} style={{
            width: '12em', background: value || '#fff',
            border: '1px dotted darkgray',
            color: '#333'
        }}>
            {value}
        </Button>
        <Popover target={`button${id}`} placement="right" isOpen={open}
            toggle={() => setOpen(!open)}>
            <SketchPicker width={300} disableAlpha={true} color={value} onChangeComplete={onChange}/>
        </Popover>
    </>;
}

export default ColorPicker;
export {ColorPicker};
