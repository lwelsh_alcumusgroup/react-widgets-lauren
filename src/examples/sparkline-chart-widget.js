import React from 'react';
import {createDefaultItem} from './dashboard-items';
import {Card, CardBody, CardText, CardTitle} from 'reactstrap';
import {Line, LineChart, ResponsiveContainer, Tooltip} from 'recharts';
import injectSheet from 'react-jss';
import color from './colors';
import {DUMMY_DATA} from './example/dummy';
import {useDesign} from '../component/contexts';
import {useContent, useEditMode, useRoot} from '../component';


const style = {
    card: {
        marginBottom: 32,
        overflow: 'hidden',
    },
    chart: {},
};

const Render = injectSheet(style)(({classes}) => {
    let [item] = useDesign();
    let [root] = useRoot();
    let [editMode] = useEditMode();
    let data = item.data.length ? item.data : DUMMY_DATA;
    const {height = 3} = item;

    return (
        <Card className={classes.card} style={{height: root.fillHeight ? 'calc(100% - 32px)' : undefined}}>
            <ResponsiveContainer height={height * 35}>
                <LineChart style={{background: item.color || '#fff'}}
                    className={classes.chart}
                    data={data}
                    margin={{top: 20, right: 10, left: 20, bottom: 0}}>
                    <Line nameKey={item.series || 'name'} strokeWidth={4} isAnimationActive={!editMode}
                        stroke={color(0, undefined, item.adjustColor)}
                        type="monotone" dataKey={item.value || 'value'}/>
                    <Tooltip labelFormatter={index => data[index][item.series || 'name']}/>
                </LineChart>
            </ResponsiveContainer>
            {(item.title || item.description) && <CardBody>
                {!!item.title && <CardTitle dangerouslySetInnerHTML={{__html: item.title}}/>}
                {!!item.description && <CardText dangerouslySetInnerHTML={{__html: item.description}}/>}
            </CardBody>}
        </Card>
    );
});

export default function(widgets) {
    widgets.render('dashboardItem.sparkline', () => {
        useContent(Render);
    });
    widgets.on('dashboardItems.types', types => {
        types.push({
            name: 'Sparkline',
            create: () => createDefaultItem({type: 'sparkline', data: [], color: '#fcfcfc'}),
        });
    });

}
