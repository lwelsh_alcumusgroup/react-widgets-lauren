import React from 'react';
import {createDefaultQuestion} from './edit-questions';
import {FormGroup, Button, Input, Label} from 'reactstrap';
import injectSheet from 'react-jss';
import {useDesign, useFocused, useValue, useWidgetContext} from '../component/contexts';
import {useContent, useContextMenu} from '../component';


const style = {
    input: {
        margin: {
            top: 8,
            bottom: 8,
        },
    },
};

const Render = injectSheet(style)(({classes}) => {
    const [text, set] = useValue();
    const [design] = useDesign();
    const {readOnly} = useWidgetContext();

    function setIfNotReadOnly(value) {
        if (readOnly) return;
        set(value);
    }

    if (!text && design.answers && design.answers.length) {
        set(design.answers[0].value);
    }

    return (
        design.question ? <FormGroup>
            <Label dangerouslySetInnerHTML={{__html: design.question}}/>
            <Input data-testid={`list-${design.name}`} readOnly={readOnly} type="select" value={text || ''}
                onChange={setIfNotReadOnly} placeholder={design.placeholder || ''}>
                {design.answers.map((answer, index) => (
                    <option key={index} value={answer.value}>{answer.text || answer.value}</option>
                ))}
            </Input>
        </FormGroup>
            : (
                <Input data-testid={`list-${design.name}`} readOnly={readOnly} className={classes.input} type="select" value={text || ''}
                    onChange={setIfNotReadOnly} placeholder={design.placeholder || ''}>
                    {design.answers.map((answer, index) => (
                        <option key={index} value={answer.value}>{answer.text || answer.value}</option>
                    ))}
                </Input>
            )
    );
});


function Answers() {
    let [question, update] = useFocused('selectedQuestion');
    if (!question || !question.answers) return <></>;
    return <FormGroup>
        <Input type="textarea"
            rows={8}
            style={{resize: 'none'}}
            placeholder="One entry per line. e.g. LABEL=value or just value"
            value={question.answers.map(answer => (answer.text ? answer.text + '=' + answer.value : answer.value || '')).join('\n')}
            onChange={(e) => {
                update({
                    answers: e.target.value.split('\n').map(l => l.trim()).map(line => {
                        let parts = line.split('=');
                        return parts.length > 1 ? {value: parts[1], text: parts[0]} : {
                            value: parts[0],
                            text: undefined,
                        };
                    })
                });
            }}/>
    </FormGroup>;
}

function Content() {
    return <Button>
        Press Me
    </Button>;
}

export default function (widgets) {

    widgets.context('survey', ()=>{
        const [question] = useFocused('selectedQuestion');
        question && question.type === 'list' && useContextMenu(Content);
    });

    widgets.render('question.list', () => {
        useContent(Render);
    });

    widgets.on('questions.types', types => {
        types.push({
            name: 'Select',
            create: () => createDefaultQuestion({type: 'list', question: 'Select a value', answers: []}),
        });
    });

    widgets.editor('survey', function ({editor, focus}) {
        focus.selectedQuestion && focus.selectedQuestion.answers && (editor.tabs.answers = {
            title: 'Possible Answers',
            content: [Answers],
        });
    });
}
