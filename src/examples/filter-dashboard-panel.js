import React, {useState} from 'react';
import {Col, FormGroup, Input, Label} from 'reactstrap';
import injectSheet from 'react-jss';
import {useBag, useDesign, useLayout} from '../component';

const styles = {
    column: {
        marginTop: 12,
        '& input': {
            fontSize: 'smaller',
            padding: '.175em .75em',
        },
    },
    filter: {
        fontSize: 'smaller',
        textTransform: 'uppercase',
        color: 'lightgray',
    },
};

const FilterPanel = injectSheet(styles)(function FilterPanel({classes}) {
    const [bag, update] = useBag();
    const [searchTerm, setSearchTerm] = useState(bag.searchTerm || '');
    return <Col className={classes.column} xs={12}>
        <FormGroup>
            <Input type="text" value={searchTerm} onChange={(e) => {
                setSearchTerm(e.target.value);
                bag.searchTerm = e.target.value || '';
                update();
            }} placeholder="Filter Dashboard..."/>
        </FormGroup>
    </Col>;
});
FilterPanel.priority = 10;

export default function (widgets) {
    widgets.on('filter.dashboardItems', function (context) {
        const [{searchTerm = ''}] = useBag();
        context.dashboardItems = context.dashboardItems.filter(item => {
            let searchString = searchTerm.toLowerCase();
            return item.title.toLowerCase().indexOf(searchString) !== -1
                || item.description.toLowerCase().indexOf(searchString) !== -1;
        });
    });

    widgets.configure('dashboard', function () {
        const [{searchTerm}] = useBag();
        const [design] = useDesign();
        if (design.dashboardItems) {
            const [,setLayout]= useLayout({headerCentre: [FilterPanel]});
            searchTerm && setLayout({footer: [FilterLabel]});
        }
    });
}

const FilterLabel = injectSheet(styles)(function FilterPanel({classes}) {
    const [bag] = useBag();
    return <Col className={classes.filter}>
        <Label>{`Filtering on: "${bag.searchTerm}"`}</Label>
    </Col>;
});

