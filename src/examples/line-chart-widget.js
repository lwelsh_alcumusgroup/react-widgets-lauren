import React from 'react';
import {createDefaultItem} from './dashboard-items';
import {Card, CardBody, CardText, CardTitle} from 'reactstrap';
import {CartesianGrid, Line, LineChart, ResponsiveContainer, XAxis, YAxis} from 'recharts';
import injectSheet from 'react-jss';
import color from './colors';
import {DUMMY_DATA} from './example/dummy';
import {useContent, useDesign, useEditMode, useRoot} from '../component';

const style = {
    card: {
        marginBottom: 32,
        overflow: 'hidden',
    },
    chart: {},
};

const Render = injectSheet(style)(({classes}) => {
    let [item] = useDesign();
    let [root] = useRoot();
    let [editMode] = useEditMode();

    let data = item.data.length ? item.data : DUMMY_DATA;
    const {height = 3} = item;
    return (

        <Card className={classes.card} style={{height: root.fillHeight ? 'calc(100% - 32px)' : undefined}}>
            <ResponsiveContainer height={height * 75}>
                <LineChart style={{background: item.color || '#fff'}}
                    className={classes.chart}
                    data={data}
                    margin={{top: 20, right: 10, left: 0, bottom: 0}}>
                    <Line nameKey={item.series || 'name'} strokeWidth={2} isAnimationActive={!editMode}
                        stroke={color(0, undefined, item.adjustColor)}
                        type="monotone"
                        dataKey={item.value || 'value'}/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey={item.series || 'name'}/>
                    <YAxis/>
                </LineChart>
            </ResponsiveContainer>
            {(item.title || item.description) && <CardBody>
                {!!item.title && <CardTitle dangerouslySetInnerHTML={{__html: item.title}}/>}
                {!!item.description && <CardText dangerouslySetInnerHTML={{__html: item.description}}/>}
            </CardBody>}
        </Card>

    );
});

export default function(widgets) {
    widgets.on('dashboardItems.types', types => {
        types.push({
            name: 'Line Chart',
            create: () => createDefaultItem({type: 'line', data: [], color: '#fcfcfc'}),
        });
    });

    widgets.render('dashboardItem.line', () => {
        useContent(Render);
    });
}
