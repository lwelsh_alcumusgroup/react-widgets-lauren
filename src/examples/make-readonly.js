import React from 'react';
import Switch from '@trendmicro/react-toggle-switch';
import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css';

import {Col} from 'reactstrap';
import injectSheet from 'react-jss';
import {useDesign, useDocument, useLayout, useWidgetContext} from '../component';

const styles = {};

const ReadOnly = injectSheet(styles)(function ReadOnly({classes}) {
    const [document, update] = useDocument();
    const {setReadOnly} = useWidgetContext();
    return <Col xs={12} className={classes.column}>
        <Switch checked={!!document.readonly} size="lg" onChange={() => {
            update({readonly: !document.readonly});
            setReadOnly(document.readonly);
        }}/>
        Read only
    </Col>;
});
ReadOnly.priority = 100;

export default function (widgets) {
    widgets.configure('survey', function () {
        const [design] = useDesign();
        if (design.questions) {
            useLayout({right: ReadOnly});
        }
    });
}

