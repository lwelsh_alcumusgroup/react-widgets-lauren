import React, {useState} from 'react';
import {createDefaultItem} from './dashboard-items';
import injectSheet from 'react-jss';
import {useFocused, useDesign, useEditMode, useContent, useTabs} from '../component';
import {Editor} from 'react-draft-wysiwyg';
import {EditorState} from 'draft-js';
import {convertFromHTML, convertToHTML} from 'draft-convert';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';



const style = {
    card: {
        marginBottom: 32,
        overflow: 'hidden',
        minHeight: 20,
        borderRadius: 4,
    },
    editor: {
        height: '12em',
        border: '1px dotted gray',
        padding: 4,
        borderRadius: 4,
        marginBottom: 16,
    },
};

const Render = injectSheet(style)(({classes}) => {
    const [item] = useDesign();
    const [editMode] = useEditMode();
    const {height = 3} = item;

    let showOutline = editMode && (!item.html || !item.html.trim().length < 8);
    return (
        <div style={Object.assign({fontSize: height * 5}, showOutline ? {
            border: '1px dotted #444',
            height: 'calc(100% - 32px)',
        } : null)} className={classes.card} dangerouslySetInnerHTML={{__html: item.html}}/>
    );
});



const HtmlEditor = injectSheet(style)(function HtmlEditor({classes}) {
    const [selectedWidget, update] = useFocused('selectedWidget');
    const [state, setState] = useState(EditorState.createWithContent(convertFromHTML(selectedWidget.html || '<p/>')));
    const [id, setId] = useState(selectedWidget.id);
    if (id !== selectedWidget.id) {
        setId(selectedWidget.id);
        setState(EditorState.createWithContent(convertFromHTML(selectedWidget.html || '<p/>')));
    }
    return (
        <div className="d-flex flex-column">
            <Editor editorClassName={classes.editor} editorState={state} onEditorStateChange={(state) => {
                try {
                    setState(state);
                    update({html: convertToHTML(state.getCurrentContent())});
                } catch (e) {
                    console.error(e);
                }

            }}/>
        </div>
    );
});

export default function(widgets) {
    widgets.on('dashboardItems.types', types => {
        types.push({
            name: 'Text',
            create: () => createDefaultItem({type: 'text'}),
        });
    });

    widgets.render('dashboardItem.text', () => {
        useContent(Render);
    });

    widgets.editor('dashboard', () => {
        const [selectedWidget] = useFocused('selectedWidget');
        if (selectedWidget && selectedWidget.type === 'text') {
            useTabs(HtmlEditor, 'Text');
        }
    });
}
