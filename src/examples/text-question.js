import React from 'react';
import {createDefaultQuestion} from './edit-questions';
import {Col, FormGroup, Input, Label} from 'reactstrap';
import injectSheet from 'react-jss';
import {useDesign, useFocused, useValue, useWidgetContext} from '../component/contexts';
import {useContent, useTabs} from '../component';

const style = {
    input: {
        margin: {
            top: 8,
            bottom: 8,
        },
    },
};

const Render = injectSheet(style)(({classes}) => {
    const [value, setValue] = useValue();
    const [design] = useDesign();
    const {readOnly} = useWidgetContext();
    return (
        design.question ? <FormGroup key={design.id}>
            <Label dangerouslySetInnerHTML={{__html: design.question}}/>
            <Input data-testid={`text-${design.name}`} readOnly={readOnly} key={design.id} type="text" value={value || ''}
                onChange={setValue} placeholder={design.placeholder || ''}/>
        </FormGroup>
            : (
                <Input data-testid={`text-${design.name}`} readOnly={readOnly} key={design.id} className={classes.input} type="text" value={value || ''}
                    onChange={setValue} placeholder={design.placeholder || ''}/>
            )
    );
});

const PlaceHolderInfo = () => {
    let [question, update] = useFocused('selectedQuestion');
    if (!question || question.placeholder === undefined) return null;
    return <FormGroup row>
        <Label sm={2}>Placeholder</Label>
        <Col sm={10}>
            <Input type="text" value={question.placeholder || ''} onFocus={e => e.target.select()}
                onChange={event => {
                    update({placeholder: event.target.value});
                }}/>
        </Col>
    </FormGroup>;
};
PlaceHolderInfo.priority = 2;

export default function (widgets) {
    widgets.on('questions.types', types => {
        types.push({
            name: 'Text',
            create: () => createDefaultQuestion({type: 'text', question: 'Text question'}),
        });
    });
    widgets.render('question.text', () => {
        useContent(Render);
    });
    widgets.editor('survey', function () {
        useTabs(PlaceHolderInfo);
    });
}
