function colors(colors) {
    return colors.match(/.{6}/g).map(function (x) {
        return '#' + x;
    });
}

const category10 = colors('1f77b4ff7f0e2ca02cd627289467bd8c564be377c27f7f7fbcbd2217becf');
const category20b = colors('393b795254a36b6ecf9c9ede6379398ca252b5cf6bcedb9c8c6d31bd9e39e7ba52e7cb94843c39ad494ad6616be7969c7b4173a55194ce6dbdde9ed6');
const category20c = colors('3182bd6baed69ecae1c6dbefe6550dfd8d3cfdae6bfdd0a231a35474c476a1d99bc7e9c0756bb19e9ac8bcbddcdadaeb636363969696bdbdbdd9d9d9');
const category20 = colors('1f77b4aec7e8ff7f0effbb782ca02c98df8ad62728ff98969467bdc5b0d58c564bc49c94e377c2f7b6d27f7f7fc7c7c7bcbd22dbdb8d17becf9edae5');

export {category10, category20, category20b, category20c};

const RGB_HEX = /^#?(?:([\da-f]{3})[\da-f]?|([\da-f]{6})(?:[\da-f]{2})?)$/i;

const hex2RGB = str => {
    const [, short, long] = String(str).match(RGB_HEX) || [];

    if (long) {
        const value = Number.parseInt(long, 16);
        return [value >> 16, (value >> 8) & 0xFF, value & 0xFF];
    } else if (short) {
        return Array.from(short, s => Number.parseInt(s, 16)).map(n => (n << 4) | n);
    }
};

function decimalToHex(d, padding) {
    var hex = Number(d).toString(16);
    padding = (typeof padding === 'undefined' || padding === null) ? 2 : padding;
    while (hex.length < padding) {
        hex = '0' + hex;
    }
    return hex;
}

export default function color(number, list = category20, offset = '#000') {
    let adjust = hex2RGB(offset);
    let base = hex2RGB(list[number % list.length]);
    return '#' + base.map((c, i) => (adjust[i] + c) % 256).map(c => decimalToHex(c)).join('');
}
