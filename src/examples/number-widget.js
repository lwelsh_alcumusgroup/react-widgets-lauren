import React from 'react';
import {createDefaultItem} from './dashboard-items';
import {Card, CardBody, CardText, CardTitle, Col, FormGroup, Input, Label} from 'reactstrap';
import injectSheet from 'react-jss';
import {DUMMY_DATA} from './example/dummy';
import {useContent, useDesign, useFocused, useRoot, useTabs} from '../component';

const style = {
    card: {
        marginBottom: 32,
        overflow: 'hidden',
    },
    chart: {
        background: 'whitesmoke',
        padding: {
            left: '0.4em',
            right: '0.4em',
            top: '0.1em',
            bottom: '0.1em',
        },
        whiteSpace: 'nowrap',
    },
};

const Render = injectSheet(style)(({classes}) => {
    const [item] = useDesign();
    const [root] = useRoot();
    let data = item.data.length ? item.data : DUMMY_DATA;
    const {height = 3} = item;
    let value;
    try {
        value = data[item.valueRow || 0][item.value || 'value'];
    } catch(e) {
        value = 'Error';
    }
    return (

        <Card className={classes.card} style={{height: root.fillHeight ? 'calc(100% - 32px)' : undefined}}>

            <div style={{
                color: item.axisColor || '#333',
                background: item.color || '#fff',
                fontSize: height * 9,
                fontWeight: 100,
                textAlign: 'right',
                width: '100%',
            }}
            className={classes.chart}>
                {value}
            </div>
            {(item.title || item.description) && <CardBody>
                {!!item.title && <CardTitle dangerouslySetInnerHTML={{__html: item.title}}/>}
                {!!item.description && <CardText dangerouslySetInnerHTML={{__html: item.description}}/>}
            </CardBody>}
        </Card>

    );
});



const DataEditor = injectSheet(style)(function DataEditor() {
    const [selectedWidget, update] = useFocused('selectedWidget');
    return (
        <div className="d-flex flex-column">
            <FormGroup row>
                <Label sm={2}>Value Row</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.valueRow || 0}
                        placeholder="Key for the labels. Defaults to 'name'" onChange={e => {
                            selectedWidget.valueRow = +e.target.value;
                            update();
                        }}/>
                </Col>
            </FormGroup>
        </div>
    );
});
DataEditor.priority = 2;


export default function(widgets) {
    widgets.on('dashboardItems.types', types => {
        types.push({
            name: 'Number',
            create: () => createDefaultItem({type: 'number', colSize: 'lg', data: [], color: '#fcfcfc'}),
        });
    });

    widgets.render('dashboardItem.number', () => {
        useContent(Render);
    });

    widgets.editor('dashboard', () => {
        const [selectedWidget] = useFocused('selectedWidget');
        if (selectedWidget && selectedWidget.type === 'number') {
            useTabs(DataEditor, 'Data');
        }
    });
}
