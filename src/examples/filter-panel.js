import React from 'react';
import {Col, FormGroup, Input, Label} from 'reactstrap';
import injectSheet from 'react-jss';
import {useBag} from '../component/contexts';
import {useDesign, useLayout, useWidgetContext} from '../component';

const styles = {
    filter: {
        fontSize: 'smaller',
        textTransform: 'uppercase',
        color: 'lightgray'
    }
};

const FilterPanel = injectSheet(styles)(function FilterPanel({classes}) {
    const [bag, updateBag] = useBag();
    return <Col className={classes.column}>
        <FormGroup>
            <Label>Filter</Label>
            <Input type="text" value={bag.searchTerm || ''} onChange={(e) => {
                updateBag({searchTerm: e.target.value});
            }} placeholder="Filter Questions..."/>
        </FormGroup>
    </Col>;
});
FilterPanel.priority = 0;

const FilterLabel = injectSheet(styles)(function FilterPanel({classes}) {
    const [bag] = useBag();
    return <Col className={classes.filter}>
        <Label>{`Filtering on: "${bag.searchTerm}"`}</Label>
    </Col>;
});

export default function(widgets) {
    widgets.on('filter.questions', function (context) {
        const [{searchTerm = ''}] = useBag();
        const {answers} = context;
        context.questions = context.questions.filter(question => {
            let searchString = searchTerm.toLowerCase();
            return (question.question || '').toLowerCase().indexOf(searchString) !== -1
                || (answers[question.name] || '').toLowerCase().indexOf(searchString) !== -1;
        });
    });

    widgets.configure('survey', function () {
        const {size} = useWidgetContext();
        const [design] = useDesign();
        const [{searchTerm}] = useBag();
        if (design.questions && size !== 'xs') {
            const [, setLayout] = useLayout({right: [FilterPanel]});
            searchTerm && setLayout({footer: FilterLabel});
        }
    });
}
