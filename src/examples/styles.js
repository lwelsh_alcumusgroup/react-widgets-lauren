const heading = {
    fontSize: 'smaller',
    fontWeight: 500,
    textTransform: 'uppercase',
    color: 'gray'
};

export {heading};
