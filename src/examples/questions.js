/* This is pretty much the whole logic for the questionnaire*/

import React from 'react';
import {arrayMoveInPlace, SortableWidget, SortableWidgetContainer} from '../component/index';
import {Col} from 'reactstrap';
import {useDesign} from '../component/contexts';
import {useDocument, useLayout} from '../component';

let useWidgets;


function Questions({questions, widgets = useWidgets, fallbackCaption, editMode, answers, ...props}) {
    let update;
    ([questions, update] = useDesign(questions));
    const questionContext = {questions, ...props, answers};
    widgets.emit('filter.questions', questionContext);
    return <SortableWidgetContainer {...props}
        isSelected={focus => focus.selectedQuestions === questions}
        axis="y"
        distance={4}
        focusOn={{selectedQuestions: questions}}
        design={questions}
        onSortEnd={({oldIndex, newIndex}) => {
            arrayMoveInPlace(questions, oldIndex, newIndex);
            update();
        }}
        document={answers}
        type="questions">
        {
            questionContext.questions.length
                ? questionContext.questions.map((question, index) => (
                    <SortableWidget {...props}
                        key={question.id || index}
                        index={index}
                        disabled={!editMode}
                        design={question}
                        document={answers}
                        value={answers[question.name]}
                        setValue={(value) => {
                            answers[question.name] = value;
                        }}
                        isSelected={focus => focus.selectedQuestion === question}
                        focusOn={{
                            selectedQuestions: questions,
                            selectedQuestion: question,
                            answers
                        }}
                        type={[`question.${question.type || 'default'}`, 'list', 'copyable']}/>))
                : (
                    <div>
                        {fallbackCaption !== undefined ? fallbackCaption : 'No questions...'}
                    </div>)
        }
    </SortableWidgetContainer>;
}

const Render = () => {
    const [design] = useDesign();
    const [document] = useDocument();
    return <Col>
        <Questions questions={design.questions} answers={document.answers}/>
    </Col>;
};

export default function (widgets) {
    useWidgets = widgets;
    widgets.configure('survey', function () {
        const [design] = useDesign();
        const [document] = useDocument();
        if (design.questions) {
            document.answers = document.answers || {};
            useLayout({content: [Render]});
        }
    });
}


export {Questions};
