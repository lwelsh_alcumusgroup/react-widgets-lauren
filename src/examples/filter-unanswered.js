import React from 'react';
import Switch from '@trendmicro/react-toggle-switch';
import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css';

import {Col} from 'reactstrap';
import {useBag} from '../component/contexts';
import injectSheet from 'react-jss';
import {useDesign, useLayout} from '../component';

const styles = {};

const FilterPanel = injectSheet(styles)(function FilterPanel({classes}) {
    const [{unanswered}, updateBag] = useBag();
    return <Col xs={12} className={classes.column}>
        <Switch checked={!!unanswered} size="lg" onChange={() => {
            updateBag({unanswered: !unanswered});
        }}/>
        Unanswered only
    </Col>;
});
FilterPanel.priority = 10;

export default function (widgets) {

    widgets.on('filter.questions', function (context) {
        const [{unanswered}] = useBag();
        const {answers, focus: {selectedQuestion} = {}} = context;
        if (unanswered) {
            context.questions = context.questions.filter(question => {
                return question === selectedQuestion || !(answers[question.name] || '').trim();
            });
        }
    });

    widgets.configure('survey', function () {
        const [design] = useDesign();
        if (design.questions) {
            useLayout({right: [FilterPanel]});
        }
    });
}

