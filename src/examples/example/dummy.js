const DUMMY_DATA = [{
    name: 'A',
    value: 50
}, {
    name: 'B',
    value: 100
}, {
    name: 'C',
    value: 75
}, {
    name: 'D',
    value: 45
}, {
    name: 'E',
    value: 175
}];

export default DUMMY_DATA;
export {DUMMY_DATA};
