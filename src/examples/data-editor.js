import React from 'react';
import injectSheet from 'react-jss';
import {globalWidgets, useTabs} from '../component';
import AceEditor from 'react-ace';
import 'brace/mode/json';
import 'brace/theme/github';
import {Col, FormGroup, Input, Label} from 'reactstrap';
import {useFocused} from '../component/contexts';

const styles = {
    editor: {
        width: '100%',
        flexGrow: 1,
    },
};

const DataEditor = injectSheet(styles)(function DataEditor() {
    const [selectedWidget, update] = useFocused('selectedWidget');
    return (
        <div className="d-flex flex-column">
            <FormGroup row>
                <Label sm={2}>Label Key</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.series || ''}
                        placeholder="Key for the labels. Defaults to 'name'" onChange={e => {
                            selectedWidget.series = e.target.value;
                            update();
                        }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Value Key</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.value || ''}
                        placeholder="Key for the numeric value to be displayed. Defaults to 'value'" onChange={e => {
                            selectedWidget.value = e.target.value;
                            update();
                        }}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label sm={2}>Data</Label>
                <Col sm={10}>
                    <AceEditor editorProps={{$blockScrolling: Infinity}} width="100%" height="18em" mode="json" theme="github"
                        value={JSON.stringify(selectedWidget.data, null, 2)}
                        onChange={(e) => {
                            try {
                                selectedWidget.data = JSON.parse(e);
                                update();
                            } catch (e) {
                                console.error(new Error(e));
                            }
                        }}/>
                </Col>
            </FormGroup>
        </div>
    );
});

export default function(widgets=globalWidgets) {
    widgets.editor('dashboard', () => {
        let [selectedWidget] = useFocused('selectedWidget');
        if (selectedWidget && selectedWidget.data) {
            useTabs(DataEditor, 'Data');
        }
    });
}
