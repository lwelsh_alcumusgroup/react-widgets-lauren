import {arrayMoveInPlace} from './index';

it('Should move an item in an array', function() {
    let items = [{a: 1}, {b: 2}];
    arrayMoveInPlace(items, 0, 1);
    expect(items[0].b).toBe(2);
    expect(items[1].a).toBe(1);
});

it('Should be able to move an item beyond the end of the array', function() {
    let items = [{a: 1}, {b: 2}];
    arrayMoveInPlace(items, 0, 5);
    expect(items[0].b).toBe(2);
    expect(items[5].a).toBe(1);
    expect(items[4]).toBe(undefined);
});
