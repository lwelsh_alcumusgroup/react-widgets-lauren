import {Col, Row} from 'reactstrap';
import React from 'react';
import {processRenderFunctions} from './utilities';

export default function layoutRow({left = [], colType = 'md', style, centreWidth, leftWidth = 3, rightWidth = 3, leftClass = '', rightClass = '', centreClass = '', centre = [], right = [], design, document, props, className}) {
    let leftInfo = {[colType]: leftWidth};
    let centreInfo = {[colType]: centreWidth || true};
    let rightInfo = {[colType]: rightWidth};
    return left.length === 0 && centre.length === 0 && right.length === 0 ? null : <Row className={className}>
        {!!left.length && <Col {...leftInfo} style={{overflowY: 'auto'}}>
            <div className={leftClass}>
                {processRenderFunctions(left).map((Item, key) => <Item key={key}/>)}
            </div>
        </Col>}
        <Col {...centreInfo} style={Object.assign({overflowY: 'auto'}, style)}>
            {!!centre.length && <div className={centreClass}>
                {processRenderFunctions(centre).map((Item, key) => <Item key={key}/>)}
            </div>}
        </Col>
        {!!right.length && <Col {...rightInfo} style={{overflowY: 'auto'}}>
            <div className={rightClass}>
                {processRenderFunctions(right).map((Item, key) => <Item key={key}/>)}
            </div>
        </Col>}
    </Row>;
}
