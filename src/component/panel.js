import {processRenderFunctions} from './utilities';
import React from 'react';
import {Col} from 'reactstrap';

function defaultChild(list, props) {
    return <Col {...props}>{list}</Col>;
}

export function Panel({visible = true, list, children, ...props}) {
    children = children || defaultChild;
    if (visible && list && list.length) {
        return children(processRenderFunctions(list).map((Item, index) =>
            <Item key={index}/>), props);
    } else {
        return null;
    }
}
