import React from 'react';
import {flushEffects, fireEvent, render} from 'react-testing-library';
import useAsync from './use-async';

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

it('should allow an async operation with a function', async ()=>{
    function Test() {
        let value = useAsync(async ()=>{
            await delay(10);
            return 1;
        });
        return <div>{value===1 ? 'Ok' : ''}</div>;
    }
    const {queryByText} = render(<Test/>);
    expect(queryByText('Ok')).toBe(null);
    await delay(50);
    expect(queryByText('Ok')).not.toBe(null);
});

it('should allow an async operation with a hard value', async () => {
    function Test() {
        let value = useAsync('1');
        return <div>{value == 1 ? 'Ok' : ''}</div>;
    }

    const {queryByText, getByText} = render(<Test/>);
    expect(queryByText('Ok')).toBe(null);
    await delay(5);
    expect(getByText('Ok')).not.toBe(null);
});
