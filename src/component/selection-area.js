import injectSheet from 'react-jss';
import {WhenSelected} from './when-selected';
import {styles} from './widget-styles';
import classnames from 'classnames';
import React from 'react';

export const SelectionArea = injectSheet(styles)(function SelectionArea({classes, visible = true}) {
    return !visible ? null : <WhenSelected>
        <div className={classnames(classes.widgetFrame, {active: true})}>
            &nbsp;
        </div>
    </WhenSelected>;
});
