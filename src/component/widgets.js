import React, {useEffect, useRef, useState} from 'react';
import injectSheet from 'react-jss';
import extractType from './type-extractor';
import {Container, Popover, PopoverBody} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import layoutRow from './widget-row';
import shortid from 'shortid';
import {FiMenu} from 'react-icons/fi';
import {useDesign, useWidgetContext, WidgetContext} from './contexts';
import SwallowClicks from './swallow-clicks';
import debounce from 'lodash/debounce';
import isEqual from 'lodash/isEqual';
import {styles} from './widget-styles';
import {calculateBreakpoint, noop} from './utilities';
import {globalWidgets} from './events';
import {useFocus} from './contexts';
import PropTypes from 'prop-types';
import {EventEmitter2} from 'eventemitter2';
import {Wrapper} from './utilities';

/**
 * Used to indicate that the Widgets content or design has changed
 * @callback OnUpdated
 */

const ContextMenu = injectSheet(styles)(function ContextMenu({classes}) {
    let context = useWidgetContext();
    let {id, widgets, contextMenu, documentType} = context;
    let [contextOpen, setContextOpen] = useState(false);
    useFocus();
    let menu = [];
    useWidgetContext.with({...context, _type: 'context', menu}, ()=>{
        widgets.emit(`context.${documentType}`, menu);
    });
    if(menu.length === 0 && contextOpen) {
        setContextOpen(false);
    }
    return <Wrapper>
        <SwallowClicks style={{display: menu.length > 0 ? 'initial' : 'none'}}>
            <div data-testid="contextopen" className={classes.contextButton} id={`context${id}`}
                onClick={()=>setContextOpen(!contextOpen)}>
                {contextMenu || <FiMenu/>}
            </div>
            <Popover placement="left" isOpen={contextOpen} target={`context${id}`}
                toggle={()=>setContextOpen(!contextOpen)}>
                <PopoverBody>
                    {menu.map((Item, index)=><Item key={index}/>)}
                </PopoverBody>
            </Popover>
        </SwallowClicks>
    </Wrapper>;
});

ContextMenu.propTypes = {
    layout: PropTypes.shape({
        headerLeft: PropTypes.arrayOf(PropTypes.any),
        headerCentre: PropTypes.arrayOf(PropTypes.any),
        headerRight: PropTypes.arrayOf(PropTypes.any),
        left: PropTypes.arrayOf(PropTypes.any),
        content: PropTypes.arrayOf(PropTypes.any),
        right: PropTypes.arrayOf(PropTypes.any),
        footer: PropTypes.arrayOf(PropTypes.any)
    }),
    className: PropTypes.any,
    id: PropTypes.any,
    onClick: PropTypes.func,
    contextMenu: PropTypes.any,
    open: PropTypes.any,
    contents: PropTypes.func
};
/**
 * Widgets must always have a `design` specified and usually a `document` if there is data capture from the user.
 * If you want to enable automatic editing then you put an [EditorFrame]{@link EditorFrame} component
 * inside.  You can also put an [Editor]{@link Editor} anywhere and bind them together using ids and a
 * shared `bag`.
 * @classdesc The main canvas for documents.  You add a Widgets component to your application as the root of
 * displaying a document and/or editing its design.
 * @kind component
 * @property {Object} design - the design of the document that will be edited by the Widgets
 * @property {Object} [document] - the document contents being edited
 * @property {string} [type] - the type of the document being edited, if this isn't specified the system tries to
 * extract it from the design or the document itself and provides an event for plugins to override or provide mapping
 * @property {boolean} [editable] - indicates whether the component can enter "edit mode"
 * @property {OnUpdated} [onUpdated] - called when the document has changed
 * @property {WidgetEvents} [widgets] - an event source to use, useful for testing, defaults to the globalWidgets event source
 * @property {Object} [style] - a style to apply to the outer div rendering the widgets
 * @property {string} [className] - a CSS class name for the outer div
 * @property {string} [leftClass] - CSS class name for the left panel
 * @property {string} [rightClass] - CSS class name for the right panel
 * @property {string} [headerLeftClass] - CSS class name for the left of the header
 * @property {string} [headerRightClass] - CSS class name for the right of the header
 * @property {string} [headerCentreClass] - CSS class name for the main header area
 * @property {string} [footerClass] - CSS class name for the footer
 * @property {string} [centreClass] - CSS class name for the main Widgets content area
 * @property {Object} [bag] - an object that is used to share data between editors and the display component, you only set
 * this when you are using an external {@link Editor} where it should share the same object
 * @property {string} [id] - an identifier for this display component, which should be matched with a similar id on the
 * {@link Editor} component if using an external editor
 * @example
 * return <Widgets design={someDesign}>
 *     <EditorFrame/>
 * </Widgets>
 * @see {@link EditorFrame} and {@link Editor} for WYSIWYG design interfaces
 */

const Widgets = injectSheet(styles)(function Widgets({widgets = globalWidgets, id: localId, bag: localBag = {}, children, contextMenu, className, footerClass, headerClass, leftClass, rightClass, contentClass, classes, style, document, design, type, editable, onUpdated = noop, ...props}) {
    if (!document || !design) {
        return <div>You must set 'document' and 'design' parameters</div>;
    }
    let documentType = extractType({type}, document, design);
    let [editMode, setEditMode] = useState(false);
    let [size, setSize] = useState(calculateBreakpoint(window.width));
    if (editMode && !editable) setEditMode(false);
    let [myBag] = useState(localBag);
    let [id] = useState(localId || shortid.generate());
    let [, doReactUpdate] = useState('update');
    let [height, setHeight] = useState(0);

    let focus = myBag.focus = myBag.focus || {};

    function setFocus(newFocus) {
        if (!isEqual(focus, newFocus)) {
            focus = myBag.focus = newFocus;
            widgets.emit(`focus.${documentType}.${id}`, newFocus);
        }
    }

    function onChanged() {
        onUpdated({document, design});
    }

    //Handle updates to the document
    useEffect(() => {
        widgets.on(`updatevalue.${documentType}.${id}.*`, onChanged);
        return function cleanup() {
            widgets.removeListener(`updatevalue.${documentType}.${id}.*`, onChanged);
        };
    });

    const queueUpdate = debounce(doReactUpdate, 150, {maxWait: 1000, leading: false, trailing: true});

    let ref = useRef(null);
    useEffect(() => {
        let newHeight = ref.current.offsetHeight;
        if (newHeight !== height) setHeight(newHeight);
        let newSize = calculateBreakpoint(ref.current.offsetWidth);
        if (newSize !== size) setSize(newSize);
        window.addEventListener('resize', queueUpdate);
        return () => {
            window.removeEventListener('resize', queueUpdate);
        };
    });


    let layout = {
        headerLeft: [],
        headerCentre: [],
        headerRight: [],
        left: [],
        content: [],
        right: [],
        footer: [],
        context: [],
    };
    let {root = design, bag = myBag} = useWidgetContext();
    let context = {
        ...props,
        id,
        size,
        documentType,
        root,
        design,
        document,
        layout,
        focus,
        setFocus,
        onChanged,
        queueUpdate,
        bag,
        editMode,
        setEditMode,
        widgets,
        height,
    };
    useWidgetContext.with({...context, _type: 'configure'}, () => {
        useDesign(design);
        widgets.emit(`configure.${documentType}`, context);
    });
    return (
        <WidgetContext.Provider
            value={context}>
            <div className={classes.holder} style={style} ref={ref}>
                <Container onClick={() => setFocus(design)} fluid={true} className={classes.widgets}>
                    {
                        /* The Header */
                        layoutRow({
                            colType: 'xs',
                            className: headerClass || classes.header,
                            left: layout.headerLeft,
                            right: layout.headerRight,
                            centre: layout.headerCentre,
                            ...context,
                        })
                    }
                    {
                        /* The Centre */
                        layoutRow({
                            style: {paddingBottom: editMode ? '50vh' : 0},
                            className: className || classes.body,
                            leftClass: leftClass || classes.left,
                            rightClass: rightClass || classes.right,
                            centreClass: contentClass || classes.content,
                            left: layout.left,
                            right: layout.right,
                            centre: layout.content,
                            ...context,
                        })
                    }
                    {
                        /* The Footer */
                        layoutRow({
                            className: footerClass || classes.footer,
                            centre: layout.footer,
                            ...context,
                        })
                    }
                    <ContextMenu/>
                    {children}
                </Container>
            </div>
        </WidgetContext.Provider>

    );
});
Widgets.propTypes = {
    design: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    document: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    widgets: PropTypes.instanceOf(EventEmitter2),
    bag: PropTypes.object,
    editable: PropTypes.bool,
    style: PropTypes.object,
    className: PropTypes.string,
    leftClass: PropTypes.string,
    rightClass: PropTypes.string,
    headerClass: PropTypes.string,
    footerClass: PropTypes.string,
    centreClass: PropTypes.string,
    onUpdated: PropTypes.func,
    type: PropTypes.string,
};

export default Widgets;
export {Widgets};
