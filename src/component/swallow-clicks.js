import React from 'react';

export default function SwallowClicks({children, style}) {
    return <div style={style} onFocus={event => event.stopPropagation()} onClick={(event) => event.stopPropagation()}>
        {children}
    </div>;
}
