import React from 'react';
import {render} from 'react-testing-library';
import {Widgets} from './widgets';
import {widgetEventsFactory} from './events';
import {useLayout} from './contexts';

let widgets = null;

beforeEach(() => {
    widgets = widgetEventsFactory();
});

it('should insert multiple items', function () {
    const One = () => <div>1</div>;
    const Two = () => <div>2</div>;
    const design = {type: 'test'}, document = {};
    widgets.configure('test', () => {
        useLayout({
            content: [One]
        });
    });
    widgets.configure('test', () => {
        useLayout({
            content: [Two]
        });
    });
    const {getByText} = render(<Widgets widgets={widgets} document={document} design={design}/>);
    expect(getByText('1'));
    expect(getByText('2'));
});

it('should allow a function to modify the list', function () {
    const One = () => <div>1</div>;
    const Two = () => <div>2</div>;
    Two.onBeforeRender = (list) => list.length = list.length - 1;
    const design = {type: 'test'}, document = {};
    widgets.configure('test', () => {
        useLayout({
            content: [One]
        });
    });
    widgets.configure('test', () => {
        useLayout({
            content: [Two]
        });
    });
    const {getByText, queryByText} = render(<Widgets widgets={widgets} document={document} design={design}/>);
    expect(queryByText('1')).not.toBe(null);
    expect(queryByText('2')).toBe(null);
});

it('should support a modified array return value', function () {
    const One = () => <div>1</div>;
    const Two = () => <div>2</div>;
    Two.onBeforeRender = () => [Two];
    const design = {type: 'test'}, document = {};
    widgets.configure('test', () => {
        useLayout({
            content: [One]
        });
    });
    widgets.configure('test', () => {
        useLayout({
            content: [Two]
        });
    });
    const {queryByText} = render(<Widgets widgets={widgets} document={document} design={design}/>);
    expect(queryByText('1')).toBe(null);
    expect(queryByText('2')).not.toBe(null);
});
