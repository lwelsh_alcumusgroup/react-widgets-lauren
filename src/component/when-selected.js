import React, {useContext} from 'react';
import {useFocus, WidgetContext} from './contexts';
import {Wrapper} from './utilities';

/**
 * WhenSelected provides a quick way to have content hide when
 * the user navigates away from the contents of a {@link Widget}
 * @kind component
 * @classdesc Use the WhenSelected component to only display content
 * when the {@link Widgets} component is focused on the parent {@link Widget}.
 * @example
 * return <WhenSelected>
 *     <span>I only appear when focused</span>
 * </WhenSelected>
 */
function WhenSelected({children}) {
    const {_isSelected} = useContext(WidgetContext);
    return useFocus(_isSelected)[0] ? <Wrapper>{children}</Wrapper> : <Wrapper/>;
}

export {WhenSelected};
