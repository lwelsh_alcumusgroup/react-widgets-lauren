import React from 'react';
import classnames from 'classnames';
import {useDesign, useFocus, useWidgetContext, WidgetContext} from './contexts';
import isEqual from 'lodash/isEqual';
import SwallowClicks from './swallow-clicks';
import {ButtonGroup, Col, Row} from 'reactstrap';
import injectSheet from 'react-jss';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import {SelectionArea} from './selection-area';
import {Panel} from './panel';
import {styles} from './widget-styles';
import PropTypes from 'prop-types';

import {ErrorBoundary} from 'react-error-boundary';

const ErrorReport = ({componentStack, error}) => (
    <div>
        <p><strong>Oops! An error occured!</strong></p>
        <p>Here’s what we know…</p>
        <p><strong>Error:</strong> {error.toString()}</p>
        {/*<p><strong>Stacktrace:</strong> {componentStack}</p>*/}
    </div>
);
const Wrapper = (props) => {
    return props.children;
};
/**
 * <p>This function is used to indicate whether the current focus setting
 * means that the Widget is selected.  The function takes the current focus
 * and should return true or false.</p>
 * <p>The function supplied should return <code>true</code> if the item is selected with the given focus.</p>
 * @callback SelectedFunction
 * @param {object} focus - the currently specified focus
 * @returns {boolean}
 * @example
 * list.map(item) => <Widget design={item} key={item.id}
 *    focusOn={{selectedItem: item, selectedItems: list}}
 *    isSelected={(focus)=>focus.selectedItem === item} />
 */

/**
 * <p>Widget components always have a design and a type, providing a specification
 * and a triggering hook point to inject functionality using the inversion
 * of control events object.</p>
 * <p>
 *     The principle is that each Widget will fire events and provide an api
 *     that the recipients of the event can use to populate part of the
 *     user interface with components that represent the contents and
 *     if desired, editors for that contents.
 * </p>
 * <p>Widget components will frequently contain other Widget components as you recurse
 * through your document's design to render all of it. You can use {@link SortableWidgetContainer} and {@link SortableWidget} to
 * represent lists of items and the individual items in a way that means the user can drag and
 * drop them to reorganise the list.</p>
 * @kind component
 * @classdesc The fundamental building block of the {@link Widgets} user interface. Each
 * Widget represents a place that functionality can be injected into the document and
 * the design.
 * @property {object} design
 * Specifies the object that this widget will
 * use to create its contents.  Normally this
 * will be a sub element of the current
 * <code>design</code> used to render the component
 * utilising the Widget<br>
 * <code>&lt;Widget design={design.questions}/&gt;</code>
 * @property {string|Array.<string>} type
 * A type or an array of types that will be used for this Widget.
 * This is how a widget finds the items to dynamically display
 * within it.
 * @property {object} [document]
 * The object that represents the document for
 * the subsection used by the widget. Normally this
 * will be a sub element of the current <code>document</code>. <br>
 * <code>&lt;Widget design={design.questions} document={document.answers}/&gt;</code>
 * @property {Any} [value]
 * For use with the useValue hook and the setValue property, this specifies what
 * the current value should be for the Widget.
 * <br>
 * This is handy if you need to map a primitive value from the `document` to the
 * Widget.
 * <br>
 * <code>
 *
 *      &lt;Widget
 *           design={item}
 *           value={document[item.id]}
 *           setValue={value=>{
 *              document[item.id] = value;
 *            }}
 *      /&gt;
 * </code>
 * @property {SetValueFunction} [setValue]
 * For use with the `useValue` hook and the `value` property, this specifies
 * how to set an updated value back into the document
 *
 * This is handy if you need to map a primitive value from the `document` to the
 * Widget.
 * @property {object} [parent]
 * Normally the parent of a widget is set automatically by
 * the system as the `design` that was in play when it was
 * created.  You can override this behaviour by specifying
 * a particular `parent`.
 * @property {SelectedFunction} [isSelected]
 * A function which indicates whether the current item should
 * be displayed as selected.  If omitted the Widget will
 * be considered selected if the current focus is exactly
 * equal to the `focusOn` prop if set, or the design otherwise.
 *
 * Frequently you will specify isSelected when you want
 * a parent item to display as selected as well as its children.
 * To do this you'll probably have both "in focus".
 * @property {object} [focusOn]
 * When this Widget is selected it will normally set the focus
 * to be the `design`, you can override that by specifying `focusOn`.
 *
 * See isSelected
 * @property {string} [endOfLineClass]
 * CSS class used for the endOfLine box - can be used to move
 * it to an absolute position etc based on where pop up
 * controls should appear.  This is normally used for editMode components
 * @property {string} [inLineClass]
 * CSS class used for the inline box (below) - can be used to move
 * it to an absolute position etc based on where pop up
 * controls should appear.  This is normally used for editMode components
 * @property {string} [className]
 * CSS class used for the content box of the widget, used for normal display.
 * @property {boolean} [requiresSelection]
 * Specifies that this widget and its children
 * should be completely redrawn when they are
 * focused or defocused.  This should be avoided
 * if at all possible
 */
const Widget = injectSheet(styles)(function Widget({classes, requiresSelection, children, design, document, parent, className = '', endOfLineClass = '', type = 'unknown', focusOn, isSelected, ...props}) {
    let types = Array.isArray(type) ? type : [type];
    const context = useWidgetContext();
    const {widgets, editMode, onChanged, bag: {focus}, setFocus} = context;

    ([design] = useDesign(design));
    parent = parent || context.design;

    document = document || context.document;
    isSelected = (isSelected || (focus => isEqual(focus, focusOn || design)));

    let inline = [];
    let endOfLine = [];
    let content = [];

    const widgetContext = {
        ...props, ...context,
        editMode,
        design,
        parent,
        document,
        _isSelected: isSelected,
        isSelected: requiresSelection ? useFocus(isSelected)[0] : isSelected(focus),
        onChanged,
        type,
        widgets,
        inline,
        endOfLine,
        content
    };
    const eventContext = {
        inline,
        endOfLine,
        content,
        design,
    };
    if (children) content.push(() => <Wrapper>{children}</Wrapper>);

    useWidgetContext.with({...widgetContext, _type: 'render'}, () => {
        types.forEach(type => {
            widgets.emit(`render.${type}`, eventContext);
        });
    });

    function processFocus(event) {
        let newFocus = focusOn || design;
        setFocus(newFocus);
        event.stopPropagation();
    }

    return <div onFocus={processFocus} onClick={processFocus}
        className={classnames(classes.widgetContext, className)}>
        <WidgetContext.Provider value={widgetContext}>
            <ErrorBoundary FallbackComponent={ErrorReport}>
                <SelectionArea visible={editMode}/>
                <Row className={classes.widgetRow}>
                    <Panel list={content}/>
                    <Panel visible={editMode} list={endOfLine}>
                        {list => (
                            <Col xs="auto" className={`d-flex align-items-center ${endOfLineClass}`}>
                                <ButtonGroup size="sm">
                                    {list}
                                </ButtonGroup>
                            </Col>)
                        }
                    </Panel>
                </Row>
                <SwallowClicks style={{position: 'relative', zIndex: 2}}>
                    <Panel visible={editMode} list={inline}/>
                </SwallowClicks>
            </ErrorBoundary>
        </WidgetContext.Provider>

    </div>;
});


export default Widget;
Widget.propTypes = {
    requiresSelection: PropTypes.bool,
    design: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    document: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    parent: PropTypes.object,
    isSelected: PropTypes.func,
    focusOn: PropTypes.object,
    type: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]).isRequired,
    endOfLineClass: PropTypes.string,
    inlineClass: PropTypes.string,
    className: PropTypes.string,
    value: PropTypes.any,
    setValue: PropTypes.func
};

export {Widget};

/**
 * Use this component inside a {@link SortableWidgetContainer} to allow for user initiated drag
 * and drop of items
 * @kind component
 * @classdesc A drag and drop sortable version of {@link Widget} to be used as each item within a sortable list
 * @augments {Widget}
 * @property {number} index - the list index number inside the sorted list
 * @property {boolean} [disabled] - if disabled the item cannot be dragged
 */
const SortableWidget = SortableElement(Widget);
SortableWidget.propTypes = {
    requiresSelection: PropTypes.bool,
    design: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    document: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    parent: PropTypes.object,
    isSelected: PropTypes.func,
    focusOn: PropTypes.object,
    type: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    endOfLineClass: PropTypes.string,
    inlineClass: PropTypes.string,
    className: PropTypes.string,
    value: PropTypes.any,
    setValue: PropTypes.func,
    index: PropTypes.number.isRequired,
    disabled: PropTypes.bool
};
/**
 * Called when sorting on a sortable list is over. Often this will just
 * pass on to {@link arrayMoveInPlace} which will move the item in
 * a standard array.  You will also need to call an `update` method
 * (probably retrieved from {@link useDesign}) to say that the list
 * has been changed afterwards.
 * @callback OnSortEnd
 * @param {number} oldIndex - the index of the item that was moved
 * @param {number} newIndex - the index of where the item was dropped
 */

/**
 * Put {@link SortableWidget}s inside this component to create a list that can be
 * arranged by user initiated drag and drop
 * @kind component
 * @classdesc A Widget that represents the list which will contain sortable items.
 * @augments {Widget}
 * @type {React.ComponentClass<any>}
 * @property {OnSortEnd} onSortEnd
 * A function that is called and passed the indices of
 * the item to move.
 * @property {number} [distance]
 * The amount of distance the pointer should be
 * dragged before starting a sort.  To avoid issues
 * with single clicks this should be more than 0
 * @property {Axis} [axis]
 * Which axes you have your widgets laid out on,
 * simple lists should just use <code>y</code> while grids
 * and dashboards normally require <code>xy</code>
 */
const SortableWidgetContainer = SortableContainer(Widget);
SortableWidgetContainer.propTypes = {
    requiresSelection: PropTypes.bool,
    design: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    document: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    parent: PropTypes.object,
    isSelected: PropTypes.func,
    focusOn: PropTypes.object,
    type: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    endOfLineClass: PropTypes.string,
    inlineClass: PropTypes.string,
    className: PropTypes.string,
    value: PropTypes.any,
    setValue: PropTypes.func,
    onSortEnd: PropTypes.func.isRequired,
    distance: PropTypes.number,
    axis: PropTypes.oneOf(['y', 'xy'])
};

export {SortableWidget, SortableWidgetContainer};
