export function noop() {
}

function inPriorityOrder(a, b) {
    return (a.priority || 1) - (b.priority || 1);
}

function processRenderFunctions(list, context) {
    list.sort(inPriorityOrder);
    if (context && context.debug) {

    }
    let renderFunctionList = list.filter(item => item.onBeforeRender);
    renderFunctionList.forEach(item => {
        let result = item.onBeforeRender(list);
        list = Array.isArray(result) ? result : list;
    });
    return list;
}

export function calculateBreakpoint(width) {
    if (width < 576) return 'xs';
    if (width < 768) return 'sm';
    if (width < 992) return 'md';
    if (width < 1200) return 'lg';
    return 'xl';
}

export {processRenderFunctions};
export {inPriorityOrder};
export const Wrapper = (props) => {
    return props.children || null;
};
