import injectSheet from 'react-jss';
import {useFocus, useWidgetContext, WidgetContext} from './contexts';
import React, {useEffect, useState} from 'react';
import {FiSettings, FiX} from 'react-icons/fi';
import {Alert, Container, Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';
import {inPriorityOrder, noop, processRenderFunctions} from './utilities';
import {styles} from './widget-styles';
import PropTypes from 'prop-types';
import {globalWidgets} from './events';
import extractType from './type-extractor';

/**
 * Used to send a message upwards to set a value
 * @callback SetValueFunction
 * @param {boolean} value - the value to set
 */

/**
 * @classdesc The EditorFrame provides an automatic popup {@link Editor} when placed inside a
 * [Widgets]{@link Widgets} component.
 * @kind component
 * @property {boolean} [editMode] - Whether the editor is in edit mode
 * @property {SetValueFunction} [setEditMode]
 * A function that can be used to set the current edit mode
 * @property {number[]} [editorMinHeight]
 * Array of numbers that are used in combination with the available height
 * to specify how large the editor should be at minimum.
 *
 * Numbers < 1 represent a proportion of the height, numbers greater than 1
 * indicate a height in pixels. Normally you will use 3 values.
 *     [idealProportion, minimumPixelHeight, noMoreThanThisProportion]
 * e.g.
 *     [0.3, 400, 0.7]
 * Ideally 30% of the height, but a minimum of 400 pixels, but no more than 70% of the height
 * @property {number[]} [editorMaxHeight]
 * Array of numbers that are used in combination with the available height
 * to specify how large the editor should be at maximum
 *
 * Numbers < 1 represent a proportion of the height, numbers greater than 1
 * indicate a height in pixels. Normally you will use 3 values.
 * @property {number} [height]
 * The height of the available space for the editor
 * @property {object} [context]
 * Context object to specify additional editor parameters
 * @property {OnUpdated} [onUpdated]
 * Called any time the design changes due to editor actions
 * @example
 * return <Widgets design={someDesign}>
 *     <EditorFrame/>
 * </Widgets>
 * @see {@link Editor} for a standalone editor
 */
export const EditorFrame = injectSheet(styles)(function EditorFrame({context = {}, ...props}) {
    context = {...useWidgetContext(), ...props, ...context};
    const {editMode, setEditMode, classes} = context;
    return !editMode ? <div data-testid="enteredit" className={classes.editorButton} onClick={() => setEditMode(true)}>
        <FiSettings/>
    </div> : <div className={classes.editor}>
        <div data-testid="exitedit" onClick={() => setEditMode(false)}
            className={classes.activeEditorButton}>
            <FiX/>
        </div>
        <Editor {...context}/>
    </div>;
});
EditorFrame.propTypes = {
    editMode: PropTypes.bool,
    setEditMode: PropTypes.func,
    editorMinHeight: PropTypes.arrayOf(PropTypes.number),
    editorMaxHeight: PropTypes.arrayOf(PropTypes.number),
    height: PropTypes.number,
    context: PropTypes.object,
    onUpdated: PropTypes.func,
};

/**
 * @kind component
 * @classdesc A standalone editor that can be bound to a {@link Widgets} component. {@link EditorFrame} provides
 * a quicker way of embedding an editor into a {@link Widgets} component.
 * @property {Object} bag - an object that is used to share data between editors and the display component, if you are
 * using an Editor then you should set the same bag on the {@link Widgets}
 * @property {string} id - an identifier for this Editor component, which should be matched with a similar id on the
 * {@link Widgets}
 * @property {Object} design - the design of the document that will be edited by this Editor
 * @property {Object} [document] - the document contents being edited
 * @property {string} [type] - the type of the document being edited, if this isn't specified the system tries to
 * extract it from the design or the document itself and provides an event for plugins to override or provide mapping
 * @property {boolean} [editMode] - Whether the editor is in edit mode
 * @property {SetValueFunction} [setEditMode]
 * A function that can be used to set the current edit mode
 * @property {number[]} [editorMinHeight]
 * Array of numbers that are used in combination with the available height
 * to specify how large the editor should be at minimum.
 *
 * Numbers < 1 represent a proportion of the height, numbers greater than 1
 * indicate a height in pixels. Normally you will use 3 values.
 *     [idealProportion, minimumPixelHeight, noMoreThanThisProportion]
 * e.g.
 *     [0.3, 400, 0.7]
 * Ideally 30% of the height, but a minimum of 400 pixels, but no more than 70% of the height
 * @property {number[]} [editorMaxHeight]
 * Array of numbers that are used in combination with the available height
 * to specify how large the editor should be at maximum
 *
 * Numbers < 1 represent a proportion of the height, numbers greater than 1
 * indicate a height in pixels. Normally you will use 3 values.
 * @property {number} [height]
 * The height of the available space for the editor
 * @property {object} [context]
 * Context object to specify additional editor parameters
 * @property {OnUpdated} [onUpdated]
 * Called any time the design changes due to editor actions
 * @property {WidgetEvents} [widgets] - an event source to use, useful for testing, defaults to the globalWidgets event source
 * @example
 * let bag = {}
 * return <div>
 *     <Widgets design={someDesign} id="example" bag={bag} type="test"/>
 *     <Editor design={someDesign} id="example" bag={bag} type="test"/>
 * </div>
 * @see {@link EditorFrame} for an easy to use plug in editor for {@link Widgets}
 */
export const Editor = injectSheet(styles)(function Editor({classes, style, editorMinHeight = [0.3, 300, 0.7], onUpdated = noop, editorMaxHeight = [0.4, 400, 0.6], height = window.innerHeight, ...context}) {
    context = {...useWidgetContext(), ...context.context, ...context};
    if (!context.bag || !context.design || !context.id) {
        return <Alert className={classes.alert} color="danger">
            Editor component not linked to a context. You should provide:
            <ul>
                <li><strong>id</strong> - an identifier shared with the component displaying the design</li>
                <li><strong>bag</strong> - an object to contain shared data between the editor and display,
                    should be set to the same value for both
                </li>
                <li><strong>design</strong> - the document being edited</li>
                <li><strong><em>type</em></strong> - either a specified value <strong>or</strong> the
                    type of the document must be discovered as being the same as
                    for the display component when the design is processed
                </li>
            </ul>
        </Alert>;
    }
    let editor = {
        header: [],
        tabs: {
            general: {
                title: <FiSettings/>,
                content: [],
            },
        },
    };

    let {widgets = globalWidgets, documentType, id, onChanged = onUpdated, type, design, bag, document} = context;
    documentType = documentType || extractType({type}, document, design);
    bag.focus = bag.focus || {};

    let minHeight = height;
    let maxHeight = height;
    /* Calculate the editor height */
    editorMinHeight = Array.isArray(editorMinHeight) ? editorMinHeight : [editorMinHeight];
    editorMaxHeight = Array.isArray(editorMaxHeight) ? editorMaxHeight : [editorMaxHeight];
    editorMinHeight.forEach(h => (h < 1) ? minHeight = Math.min(minHeight, height * h) : minHeight = Math.max(minHeight, h));
    editorMaxHeight.forEach(h => (h < 1) ? maxHeight = Math.min(maxHeight, height * h) : maxHeight = Math.max(maxHeight, h));

    //Handle updates to the document
    useEffect(() => {
        widgets.on(`update.${documentType}.${id}.*`, onChanged);
        return function cleanup() {
            widgets.removeListener(`update.${documentType}.${id}.*`, onChanged);
        };
    });

    let [currentTab, setTab] = useState('general');
    editor.tabs = {
        general: {
            title: <FiSettings/>,
            content: [],
        },
    };
    context.documentType = documentType;
    context.widgets = widgets;
    context.editor = editor;
    useWidgetContext.override = {...context, _type: 'editor'};
    let [, focus] = useFocus();
    context.focus = focus;

    widgets.emit(`editor.${documentType}`, context);

    if (!editor.tabs[currentTab]) {
        currentTab = 'general';
    }

    let result = <WidgetContext.Provider value={context}>
        <div onClick={(event) => event.stopPropagation()}>
            <div className={classes.editorInner}>
                <Nav tabs>
                    {Object.entries(editor.tabs).sort(inPriorityOrder).map(([key, tab]) => {
                        return <NavItem className={classes.tab} key={key}>
                            <NavLink
                                className={classnames({active: currentTab === key})}
                                onClick={() => setTab(key)}>
                                {tab.title}
                            </NavLink>
                        </NavItem>;
                    })}
                </Nav>
                <TabContent style={{maxHeight, minHeight, ...style}}
                    className={classes.editorContent} activeTab={currentTab}>
                    {Object.entries(editor.tabs).map(([key, tab]) => {
                        let children = processRenderFunctions(tab.content, context).map((Child, key) =>
                            <Child key={key} tab={tab}/>);
                        return <TabPane key={key} tabId={key}>
                            <Container>
                                <br/>
                                {children}
                            </Container>
                        </TabPane>;
                    })}
                </TabContent>
            </div>
        </div>
    </WidgetContext.Provider>;
    useWidgetContext.override = null;
    return result;

});
Editor.propTypes = {
    id: PropTypes.string.isRequired,
    design: PropTypes.object.isRequired,
    bag: PropTypes.object.isRequired,
    type: PropTypes.string,
    editMode: PropTypes.bool,
    setEditMode: PropTypes.func,
    editorMinHeight: PropTypes.arrayOf(PropTypes.number),
    editorMaxHeight: PropTypes.arrayOf(PropTypes.number),
    height: PropTypes.number,
    context: PropTypes.object,
    onUpdated: PropTypes.func,
};
