import {globalWidgets} from './events';

function extractType(...params) {
    const typeData = {type: params.reduce((p,c)=>{
        return (p || c.type);
    }, undefined) || 'unknown', priority: 1};
    globalWidgets.emit('getType', document, typeData);
    return typeData.type;
}

export default extractType;
