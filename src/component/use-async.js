import {useState} from 'react';
import isFunction from 'lodash/isFunction';

const GUARD_VALUE = '_guard_';

/**
 * Returns null until some value or async function is resolved,
 * causes a React redraw when resolved.
 * @param {(Function|Promise|any)} promiseProducingFunction - either an async function, a promise or a value
 * @returns any
 * @example
 * function App() {
   let hasComponents = useAsync(async () => {
        try {
            await Promise.all(components.map(async item => {
                let module = await import(`./examples/${item}`)
                module.default(globalWidgets)
            }))
        } catch(e) {
            console.error(e.stack)
        }
        return true
    })
    return !hasComponents ? <div>Wait...</div> : <Widgets design={design} document={document} type="someType"/>
}

 */
function useAsync (promiseProducingFunction) {
    let [value, setValue] = useState(GUARD_VALUE);
    if (value === GUARD_VALUE) {
        let tempValue = promiseProducingFunction;
        promiseProducingFunction = !isFunction(promiseProducingFunction) ? async () => tempValue : promiseProducingFunction;
        setValue(null);
        value = null;
        Promise.resolve(promiseProducingFunction()).then(v => {
            setValue(v);
        });

    }
    return value;
}

export {useAsync};
export default useAsync;
