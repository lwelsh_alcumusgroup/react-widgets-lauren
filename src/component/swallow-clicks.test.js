import React from 'react';
import {fireEvent, render} from 'react-testing-library';
import SwallowClicks from './swallow-clicks';

it('Should swallow clicks', async () => {
    const callBack = jest.fn();
    const {getByTestId} = render(<div onClick={callBack}>
        <SwallowClicks>
            <button data-testid="button">button</button>
        </SwallowClicks>
    </div>);
    fireEvent.click(getByTestId('button'));
    expect(callBack).toHaveBeenCalledTimes(0);
});

it('Should swallow focus', async () => {
    const callBack = jest.fn();
    const {getByTestId} = render(<div onFocus={callBack}>
        <SwallowClicks>
            <input data-testid="button"/>
        </SwallowClicks>
    </div>);
    fireEvent.focus(getByTestId('button'));
    expect(callBack).toHaveBeenCalledTimes(0);
});

