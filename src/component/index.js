import {Widgets} from './widgets';
import {inPriorityOrder} from './utilities';
import {useAsync} from './use-async';
import {
    useBag,
    useContent,
    useContextMenu,
    useDesign,
    useDocument,
    useEditMode,
    useEndOfLine,
    useEvents,
    useFocus,
    useFocused,
    useInline,
    useLayout,
    useParent,
    useRoot,
    useTabs,
    useValue,
    useWidgetContext,
} from './contexts';

export {Widgets};

export {
    useAsync,
    useLayout,
    useValue,
    useBag,
    useContextMenu,
    useContent,
    useWidgetContext,
    useTabs,
    useRoot,
    useParent,
    useInline,
    useFocused,
    useFocus,
    useEvents,
    useEndOfLine,
    useEditMode,
    useDocument,
    useDesign,
};
import {Widget, SortableWidget, SortableWidgetContainer} from './widget-component';

export {Widget, SortableWidgetContainer, SortableWidget};
import {globalWidgets, widgetEventsFactory} from './events';
export {globalWidgets, widgetEventsFactory};
import {Editor, EditorFrame} from './editor';
export {Editor, EditorFrame};
export {inPriorityOrder};

/**
 * Helper function to rearrange an array, useful when used with {@link SortableWidgetContainer} in the onSortEnd method
 * @param {Array} array - the array to modify
 * @param {number} previousIndex - the previous index
 * @param {number} newIndex - the new index of the item
 * @example
 return <SortableWidgetContainer {...props}
 isSelected={focus => focus.selectedQuestions === questions}
 axis="y"
 distance={4}
 focusOn={{selectedQuestions: questions}}
 design={questions}
 onSortEnd={({oldIndex, newIndex}) => {
                                            arrayMoveInPlace(questions, oldIndex, newIndex)
                                            update()
                                        }}
 document={answers}
 type="questions">
 {items}
 </SortableWidgetContainer>
 */
export function arrayMoveInPlace(array, previousIndex, newIndex) {
    if (newIndex >= array.length) {
        let k = newIndex - array.length;

        while (k-- + 1) {
            array.push(undefined);
        }
    }
    array.splice(newIndex, 0, array.splice(previousIndex, 1)[0]);
}


export default Widgets;
