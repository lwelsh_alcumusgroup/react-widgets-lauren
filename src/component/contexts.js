import React, {useContext, useEffect, useState} from 'react';
import shortid from 'shortid';
import isObject from 'lodash/isObject';
import isFunction from 'lodash/isFunction';
import merge from 'lodash/merge';
import mergeWith from 'lodash/mergeWith';

/**
 * Any React component
 * @typedef {function|Class} ReactComponent
 */
/**
 * Adds a ReactComponent to the list to be rendered
 * @callback addItem
 * @param {ReactComponent} component - the component to add
 */

/**
 * Return from a useLayout
 * @typedef ComponentListReturn
 * @type {array}
 * @property {ReactComponent[]} 0 - the current components
 * @property {addItem} 1 - a function to add a new item to the list
 */

/**
 * A description of the layout canvas of a Widgets framework
 * @typedef {Object} Layout
 * @property {ReactComponent[]} headerLeft - Top left components
 * @property {ReactComponent[]} headerCentre - Header centre components
 * @property {ReactComponent[]} headerRight - Top right components
 * @property {ReactComponent[]} left - Left panel components
 * @property {ReactComponent[]} content - Core {@link Widgets} contents
 * @property {ReactComponent[]} right - Right panel components
 * @property {ReactComponent[]} footer - Footer components
 */

/**
 * Sets the layout by merging a layout definition into the existing
 * layout
 * @callback setLayout
 * @param {Layout} layout - the layout to merge in
 */

/**
 * Return from a useLayout
 * @typedef LayoutReturn
 * @type {array}
 * @property {Layout} 0 - the current layout
 * @property {setLayout} 1 - a function to merge in a new layout
 */


const WidgetContext = React.createContext({design: null, document: null});
const EditorContext = React.createContext(null);

function noop() {
}

function useFocus(isSelected = noop) {
    if (!isFunction(isSelected)) {
        throw new Error('Wrong parameter to useFocus, did you mean useFocused?');
    }
    let {widgets, id, documentType, bag} = useWidgetContext();
    let focus = bag ? bag.focus : {};
    let [, setSelected] = useState(false);
    let currentSelectedState = isSelected(focus);

    function focusChange(focus) {
        let selected = isSelected(focus);
        if (currentSelectedState === undefined || selected !== currentSelectedState) {
            currentSelectedState = selected;
            setSelected(selected);
        }
    }

    useEffect(() => {
        widgets.addListener(`focus.${documentType}.${id}`, focusChange);
        return function cleanup() {
            widgets.removeListener(`focus.${documentType}.${id}`, focusChange);
        };
    });
    return [currentSelectedState, focus];
}

const DUMMY = {};

/**
 * Return the part of the current focus that should be used
 * @callback FocusSelection
 * @param {object} focus - the current focus of the {@link Widgets}
 * @returns {object}
 */

/**
 * <p>
 *     Used a lot within [editor]{@link WidgetEvents#editor} events, you use this
 *     hook to get a currently focused item.
 * </p>
 * @param {FocusSelection|string} selector - the item that should be selected, either a function that takes
 * the current focus and returns the item or a string specifying the property of the focus to use
 * @returns {UseDesignReturn|Array}
 * @example
 widgets.editor("dashboard", () => {
        const [selectedWidget] = useFocused("selectedWidget")
        if (selectedWidget && selectedWidget.type === "number") {
            useTabs(DataEditor, "Data")
        }
    })

 */
function useFocused(selector) {
    let [, focus] = useFocus();
    let value = isFunction(selector) ? selector(focus) : (selector ? focus[selector] : focus);
    if (!value) {
        useDesign(DUMMY);
        return [null, noop];
    } else {
        return useDesign(value);
    }
}

export {useFocused};
/**
 * The MergeUpdate method takes a new object describing changed
 * properties and merges it into the existing state, firing
 * an update event.  Arrays in the merge will be replaced
 * @callback MergeUpdate
 * @param {object} [value] - merge the passed value into the existing value
 */

/**
 * @typedef UseDesignReturn
 * @type {array}
 * @property {object} 0 - The current document
 * @property {MergeUpdate} 1 - Called to merge updates into the design
 */

/**
 * <p>Provides access to the current <code>design</code> which normally
 * represents the structure of the document for the current recursive
 * point during the rendering.</p>
 * <p>See also {@link useDocument} for user entered values.</p>
 * @param {object} [design] - optional override for the currently in scope design
 * @returns {UseDesignReturn|Array}
 */
function useDesign(design = DUMMY) {
    const context = useWidgetContext();
    const [, setDesignReact] = useState();
    design = design === DUMMY ? context.design : (design || DUMMY);
    design.id = design.id || shortid.generate();
    const {id, documentType} = context;
    const designId = design.id;

    function willUpdate() {
        setDesignReact();
    }

    //Ensure this component refreshes if something else edits the design
    useEffect(() => {
        context.widgets.on(`update.${documentType}.${id}.${designId}`, willUpdate);
        return function cleanup() {
            context.widgets.removeListener(`update.${documentType}.${id}.${designId}`, willUpdate);
        };
    });
    return [design === DUMMY ? null : design, function setDesign(value = null) {
        if (isObject(value)) {
            merge(design, value);
        }
        context.widgets.emit(`update.${documentType}.${id}.${designId}`, design);
    }, context];
}

/**
 * @typedef UseBagReturn
 * @type {array}
 * @property {object} 0 - The bag for the {@link Widgets} component
 * @property {MergeUpdate} 1 - A function to merge updates into the bag and redraw
 */

/**
 * Provides access to the "Bag" which is a shared data object that the UI can use to
 * keep track of values and communicate between components.
 * @returns {UseBagReturn|Array}
 * @example
 const FilterPanel = injectSheet(styles)(function FilterPanel({classes}) {
    const [bag, updateBag] = useBag()
    return <Col className={classes.column}>
        <FormGroup>
            <Label>Filter</Label>
            <Input type="text" value={bag.searchTerm || ""} onChange={(e) => {
                updateBag({searchTerm: e.target.value})
            }} placeholder="Filter Questions..."/>
        </FormGroup>
    </Col>
})
 FilterPanel.priority = 0
 */
function useBag() {
    let [, update, {bag, widgets, id, documentType}] = useDesign();
    return [bag, function (value) {
        if (isObject(value)) {
            merge(bag, value);
        }
        widgets.emit(`updatebag.${documentType}.${id}`, bag);
        update();
    }];
}

/**
 * @typedef UseParentReturn
 * @type {array}
 * @property {object} 0 - The parent of the current design
 * @property {function} 1 - A function that will cause the parent and its children to redraw
 */

/**
 * Provides access to the parent of the current design while rendering.  This is handy for
 * redrawing lists if the order of items change or items are added or deleted
 * @returns {UseParentReturn|Array}
 */
function useParent() {
    const context = useWidgetContext();
    return [context.parent, function () {
        context.widgets.emit(`update.${context.documentType}.${context.id}.${context.parent.id}`, context.parent);
    }, context];
}


/**
 * @typedef UseDocumentReturn
 * @type {array}
 * @property {object} 0 - The current document
 * @property {MergeUpdate} 1 - Called to merge updates into the document

 */

/**
 * <p>Provides access to the current <code>document</code> which normally
 * represents the data entered by the user for the current recursive
 * point during the rendering.</p>
 * <p>See also {@link useValue} when representing recursed values in rendering components.</p>
 * @param {object} [document] - optional override for the currently in scope document
 * @returns {UseDocumentReturn|Array}
 */
function useDocument(document) {
    const context = useWidgetContext();
    const [, setDocumentReact] = useState();
    const {design, id, documentType} = context;
    document = document || context.document;
    design.id = design.id || shortid.generate();

    function willUpdate() {
        setDocumentReact();
    }

    //Ensure this component refreshes if something else edits the design
    useEffect(() => {
        context.widgets.on(`updatevalue.${documentType}.${id}.${design.id}`, willUpdate);
        return function cleanup() {
            context.widgets.removeListener(`updatevalue.${documentType}.${id}.${design.id}`, willUpdate);
        };
    });
    return [document, function setDocument(value = null) {
        if (isObject(value)) {
            merge(document, value);
        }
        context.widgets.emit(`updatevalue.${documentType}.${id}.${design.id}`, document);
    }, context];
}

/**
 * @callback SetValueFromEventOrParamFunction
 * @param {any|Event} value - sets the value either to the contents of <code>event.target.value</code> if passed an
 * event, or the actual value passed in otherwise
 */

/**
 * @typedef UseValueReturn
 * @type {array}
 * @property {any} 0 - The current value
 * @property {SetValueFromEventOrParamFunction} 1 - The function to set the value, can take a value or an event

 */

/**
 * <p>
 *     Works with the {@link Widget} component's ability to specify <code>value</code> and <code>setValue</code>
 *     props which have a widget representing some primitive value.  The useValue hook lets you access and update
 *     the value from a renderer.
 * </p>
 * <p>
 *     The setValue function is capable of taking a primitive value of an event from an input.
 * </p>
 * @returns {UseValueReturn|Array}
 * @example
 const Render = injectSheet(style)(({classes}) => {
    const [value, setValue] = useValue()
    const [design] = useDesign()
    const {readOnly} = useWidgetContext()
    return (
        design.question ? <FormGroup key={design.id}>
                <Label>{design.question}</Label>
                <Input readOnly={readOnly} key={design.id} type="text" value={value || ""}
                       onChange={setValue} placeholder={design.placeholder || ""}/>
            </FormGroup>
            : (
                <Input readOnly={readOnly} key={design.id} className={classes.input} type="text" value={value || ""}
                       onChange={setValue} placeholder={design.placeholder || ""}/>
            )
    )
})

 */
function useValue() {
    const context = useWidgetContext();
    const {value, setValue} = context;
    const [useValue, setUseValue] = useState(value);
    if (!setValue) {
        throw new Error('Unable to use value on a context that does not contain a setValue method');
    }
    const [, update] = useDocument();
    return [useValue, function (value) {
        if (value.target) {
            value = value.target.value;
        }
        if (useValue !== value) {
            setValue(value);
            setUseValue(value);
            update();
        }
    }];
}

/**
 * Returns the current widget context, which contains every parameter passed to the {@link Widgets} component.
 * So can be handy for accessing important external functions or values.
 * @returns {Object}
 * @example
 *     const {readOnly} = useWidgetContext()
 */
function useWidgetContext() {
    return useWidgetContext.override || useContext(WidgetContext);
}

useWidgetContext.with = function (context, fn) {
    useWidgetContext.override = context;
    fn();
    useWidgetContext.override = null;
};

/**
 * Return from a useLayout
 * @typedef EditModeReturn
 * @type {array}
 * @property {boolean} 0 - whether the rendering is in "edit mode"
 * @property {update} 1 - a function that will refresh the whole {@link Widgets} component
 */

/**
 * Returns whether the current rendering is in edit mode
 * and provides a refresh function
 * @returns {EditModeReturn|Array}
 */
function useEditMode() {
    const context = useWidgetContext();
    return [context.editMode, context.queueUpdate];
}

/**
 * Returns the root design that the underlying {@link Widgets} component
 * is rendering. This can be useful as sometimes you need to access
 * global properties of the design
 * @returns {Object}
 */
function useRoot() {
    const context = useWidgetContext();
    return [context.root, context.queueUpdate];
}

/**
 * Returns the events associated with the current rendering process
 * @returns {WidgetEvents}
 */
function useEvents() {
    const context = useWidgetContext();
    return context.widgets;
}

/**
 * Provides a structure to hold the tabs.
 * ```jsx
 *     {
 *         general: {
 *             title: <FiSettings/>,
 *             content: []
 *         },
 *         "Example New Tab": {
 *             title: "Example New Tab",
 *             content: [Component1, Component2]
 *         }
 *     }
 * ```
 * @typedef Tabs
 */

/**
 * @callback AddTab
 * @param {ReactComponent} [newTab] - the component to add to the tab
 * @param {string} [title="General"] - Title of the tab to add the content to, defaults to the general tab
 * @param {string} [tab=title] - name of the tab, names are usually the same as the title, but it can be used as a different value if desired
 * @example
 //Add some tabs to the "General" page
 widgets.editor("dashboard", function () {
    const [selectedWidget] = useFocused("selectedWidget")
    const [, addTab] = useTabs(StretchInfo)
    selectedWidget && addTab(DashboardInfo)
 })
 */

/**
 * @typedef TabLayout
 * @type {array}
 * @property {Tabs} 0 - the current layout
 * @property {AddTab} 1 - a function to add a new Tab
 */

/**
 * <p>
 *     Allows one or more tabs to be added to the editor user interface. You can either use
 *     the shortcut properties to quickly add a tab, or take the return values and
 *     add more than one.
 * </p>
 * @param {ReactComponent} [newTab] - the component to add to the tab
 * @param {string} [title="General"] - Title of the tab to add the content to, defaults to the general tab
 * @param {string} [tab=title] - name of the tab, names are usually the same as the title, but it can be used as a different value if desired
 * @returns {(TabLayout|Array)}
 * @example
 //Add some tabs to the "General" page
 widgets.editor("dashboard", function () {
    const [selectedWidget] = useFocused("selectedWidget")
    const [, addTab] = useTabs(StretchInfo)
    selectedWidget && addTab(DashboardInfo)
 })
 */
function useTabs(newTab, title, tab) {
    const context = useWidgetContext();
    if (context._type !== 'editor') throw new Error('Only call useTabs in an editor handler');
    let {editor: {tabs}} = context;
    let result = [tabs, function addTab(content, title = 'general', tab = title) {
        let tabItem = tabs[tab] = tabs[tab] || {
            title,
            content: [],
        };
        tabItem.content.push(content);
    }];
    if (newTab) {
        result[1](newTab, title, tab);
    }
    return result;
}

/**
 * This hook is used to add items that render after the Widget's content, normally
 * on a subsequent line if you don't move them by specifying a CSS class on the
 * Widget component in the `inlineClass` property.
 *
 * @param {ReactComponent} [newContent] - optional component to add to the list
 * @returns {(ComponentListReturn|Array)}
 * @example
 * widgets.render("questions", function () {
        let widgets = useEvents()
        let types = []
        widgets.emit("questions.types", types)
        useInline(props => <AddQuestion types={types} {...props}/>)
   })
 */

function useInline(newContent) {
    const context = useWidgetContext();
    if (context._type !== 'render') throw new Error('Only call useInline in a render handler');
    let {inline} = context;
    let result = [inline, function addItem(content) {
        inline.push(content);
    }];
    if (newContent) {
        result[1](newContent);
    }
    return result;
}

/**
 * This hook is used to add items that render the context menu of a Widget.
 *
 * @param {ReactComponent} [newMenuItem] - optional component to add to the list of context items
 * @returns {(ComponentListReturn|Array)}
 * @example
 * widgets.context("dashboardItem.pie", () => {
 *    useContextMenu(ContextMenu)
 * })
 * //or
 * widgets.context("dashboardItem.pie", () => {
 *    const [menu, addMenuItem] = useContextMenu()
 *    if(menu.length < 2) addMenuItem(ExtraMenuItem)
 * })
 */

function useContextMenu(newMenuItem) {
    const {menu, _type} = useWidgetContext();
    if (_type !== 'context') throw new Error('Only call useContextMenu in a context handler');
    let result = [menu, function addItem(newMenuItem) {
        menu.push(newMenuItem);
    }];
    if (newMenuItem) {
        result[1](newMenuItem);
    }
    return result;
}


/**
 * This hook is used to add items that render the content of a Widget.
 * It is the primary function of the widget to display this content
 * outside of edit mode
 *
 * @param {ReactComponent} [newContent] - optional component to add to the list
 * @returns {(ComponentListReturn|Array)}
 * @example
 * widgets.render("dashboardItem.pie", () => {
 *    useContent(Render)
 * })
 * //or
 * widgets.render("dashboardItem.pie", () => {
 *    const [existingContents, addContent] = useContent()
 *    if(existingContents.length < 2) addContent(Render)
 * })
 */

function useContent(newContent) {
    const context = useWidgetContext();
    if (context._type !== 'render') throw new Error('Only call useContent in a render handler');
    let {content} = context;
    let result = [content, function addItem(newContent) {
        content.push(newContent);
    }];
    if (newContent) {
        result[1](newContent);
    }
    return result;
}

function mergeArrays(objValue, srcValue) {
    if (Array.isArray(objValue)) {
        return objValue.concat(Array.isArray(srcValue) ? srcValue : [srcValue]);
    }
}


/**
 * This hook is used to add items to the "layout canvas" of a Widgets
 * component.  You must use this only when responding to
 * a [configure]{@link Events#configure} event to place items in the content area or one of
 * the other slots in the canvas.
 * @param {Layout} [layoutToMerge] - optional new layout to merge in
 * @returns {(LayoutReturn|Array)}
 * @example
 widgets.configure("survey", function () {
        const [design] = useDesign()
        if (design.questions) {
            useLayout({right: ReadOnly})
        }
     })
 */
function useLayout(layoutToMerge) {
    const context = useWidgetContext();
    if (context._type !== 'configure') throw new Error('Only call useLayout in a configure handler');
    let {layout} = context;
    let result = [layout, function setLayout(newLayout = {}) {
        mergeWith(layout, newLayout, mergeArrays);
    }];
    if (layoutToMerge) {
        result[1](layoutToMerge);
    }
    return result;
}


/**
 * This hook is used to add items to the an area to the right of
 * a Widget's contents.  Normally used for edit mode components like
 * trashing an item or moving it. It can only be used in a `render` event.
 *
 * You can move the location of the endOfLine items by passing a
 * CSS class to style them differently.
 *
 * @param {ReactComponent} [newContent] - optional component to add to the list
 * @returns {(ComponentListReturn|Array)}
 * @example
 widgets.render("copyable", function () {
         useEndOfLine(Render)
     })
 */
function useEndOfLine(newContent) {
    const context = useWidgetContext();
    let {endOfLine} = context;
    let result = [endOfLine, function addItem(content) {
        endOfLine.push(content);
    }];
    if (newContent) {
        result[1](newContent);
    }
}

export {
    WidgetContext,
    EditorContext,
    useInline,
    useLayout,
    useContent,
    useEndOfLine,
    useEvents,
    useTabs,
    useWidgetContext,
    useFocus,
    useDesign,
    useBag,
    useDocument,
    useValue,
    useParent,
    useEditMode,
    useRoot,
    useContextMenu
};
