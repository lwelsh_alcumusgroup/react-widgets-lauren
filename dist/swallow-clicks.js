"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = SwallowClicks;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function SwallowClicks(_ref) {
  var children = _ref.children,
      style = _ref.style;
  return _react.default.createElement("div", {
    style: style,
    onFocus: function onFocus(event) {
      return event.stopPropagation();
    },
    onClick: function onClick(event) {
      return event.stopPropagation();
    }
  }, children);
}