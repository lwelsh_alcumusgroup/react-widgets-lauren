"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactTestingLibrary = require("react-testing-library");

var _useAsync = _interopRequireDefault(require("./use-async"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function delay(time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

it("should allow an async operation with a function",
/*#__PURE__*/
_asyncToGenerator(
/*#__PURE__*/
regeneratorRuntime.mark(function _callee2() {
  var Test, _render, queryByText;

  return regeneratorRuntime.wrap(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          Test = function _ref3() {
            var value = (0, _useAsync.default)(
            /*#__PURE__*/
            _asyncToGenerator(
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return delay(10);

                    case 2:
                      return _context.abrupt("return", 1);

                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            })));
            return _react.default.createElement("div", null, value === 1 ? 'Ok' : "");
          };

          _render = (0, _reactTestingLibrary.render)(_react.default.createElement(Test, null)), queryByText = _render.queryByText;
          expect(queryByText("Ok")).toBe(null);
          _context2.next = 5;
          return delay(50);

        case 5:
          expect(queryByText("Ok")).not.toBe(null);

        case 6:
        case "end":
          return _context2.stop();
      }
    }
  }, _callee2, this);
})));
it("should allow an async operation with a hard value",
/*#__PURE__*/
_asyncToGenerator(
/*#__PURE__*/
regeneratorRuntime.mark(function _callee3() {
  var Test, _render2, queryByText, getByText;

  return regeneratorRuntime.wrap(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          Test = function _ref5() {
            var value = (0, _useAsync.default)("1");
            return _react.default.createElement("div", null, value == 1 ? 'Ok' : "");
          };

          _render2 = (0, _reactTestingLibrary.render)(_react.default.createElement(Test, null)), queryByText = _render2.queryByText, getByText = _render2.getByText;
          expect(queryByText("Ok")).toBe(null);
          _context3.next = 5;
          return delay(5);

        case 5:
          expect(getByText("Ok")).not.toBe(null);

        case 6:
        case "end":
          return _context3.stop();
      }
    }
  }, _callee3, this);
})));