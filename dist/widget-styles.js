"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = exports.color = void 0;
var editorButton = {
  cursor: "pointer",
  padding: {
    left: "1em",
    right: "1em",
    top: "0.7em",
    bottom: "0.7em"
  },
  background: "whitesmoke",
  color: "gray",
  position: "absolute",
  borderRadius: "4px 4px 0 0",
  right: "1em"
};
var color = "rgba(138, 192, 250, 0.075)";
exports.color = color;
var styles = {
  alert: {
    margin: "1em"
  },
  holder: {
    display: "flex",
    height: "100%",
    position: "relative",
    overflow: "hidden"
  },
  contextButton: {
    cursor: "pointer",
    padding: {
      left: "0.8em",
      right: "0.8em",
      top: "0.5em",
      bottom: "0.5em"
    },
    fontSize: "larger",
    fontWeight: "bold",
    background: "whitesmoke",
    color: "gray",
    position: "absolute",
    borderRadius: "4px 0px 0px 4px",
    right: 0,
    top: "20%",
    opacity: 0.76
  },
  tab: {
    cursor: "pointer"
  },
  widgets: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    minHeight: "100%",
    overflowY: "auto",
    "& *": {
      userSelect: "none"
    },
    "& input, & textarea": {
      userSelect: "initial"
    }
  },
  header: {
    background: "whitesmoke",
    paddingTop: 8,
    paddingBottom: 8,
    flexShrink: 0
  },
  left: {
    background: "darkgray",
    color: "whitesmoke",
    alignContent: "flex-start"
  },
  content: {},
  right: {
    background: "#555",
    color: "whitesmoke",
    height: "100%",
    alignContent: "flex-start"
  },
  body: {
    margin: {
      left: -15,
      right: -15
    },
    flexGrow: 1
  },
  spacer: {
    height: "40vh"
  },
  footer: {
    flexShrink: 0
  },
  editor: {
    boxShadow: {
      blur: 20
    },
    position: "absolute",
    bottom: 0,
    background: "white",
    border: "none",
    minHeight: "32px",
    left: 0,
    right: 0,
    zIndex: 5
  },
  editorContent: {
    maxHeight: "50vh",
    overflowY: "auto",
    background: "white"
  },
  editorInner: {
    position: "relative",
    background: "whitesmoke",
    paddingTop: 4,
    width: "100%"
  },
  editorButton: Object.assign({
    opacity: 0.6,
    bottom: 0
  }, editorButton),
  activeEditorButton: Object.assign({
    top: 1,
    transform: "translateY(-100%)",
    opacity: 1,
    boxShadow: {
      blur: 6,
      y: -6
    },
    zIndex: 4
  }, editorButton),
  outer: {
    height: "100%"
  },
  widgetRow: {
    minHeight: 31,
    height: "100%"
  },
  widgetContext: {
    position: "relative"
  },
  widgetFrame: {
    position: "absolute",
    left: -4,
    right: -4,
    top: -4,
    bottom: -4,
    border: "none",
    borderRadius: 4,
    "&.active": {
      background: color
    }
  }
};
exports.styles = styles;