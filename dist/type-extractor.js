"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _events = require("./events");

function extractType() {
  for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
    params[_key] = arguments[_key];
  }

  var typeData = {
    type: params.reduce(function (p, c) {
      return p || c.type;
    }, undefined) || "unknown",
    priority: 1
  };

  _events.globalWidgets.emit("getType", document, typeData);

  return typeData.type;
}

var _default = extractType;
exports.default = _default;