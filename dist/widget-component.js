"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SortableWidgetContainer = exports.SortableWidget = exports.Widget = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _contexts = require("./contexts");

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var _swallowClicks = _interopRequireDefault(require("./swallow-clicks"));

var _reactstrap = require("reactstrap");

var _reactJss = _interopRequireDefault(require("react-jss"));

var _reactSortableHoc = require("react-sortable-hoc");

var _selectionArea = require("./selection-area");

var _panel = require("./panel");

var _widgetStyles = require("./widget-styles");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactErrorBoundary = require("react-error-boundary");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var ErrorReport = function ErrorReport(_ref) {
  var componentStack = _ref.componentStack,
      error = _ref.error;
  return _react.default.createElement("div", null, _react.default.createElement("p", null, _react.default.createElement("strong", null, "Oops! An error occured!")), _react.default.createElement("p", null, "Here\u2019s what we know\u2026"), _react.default.createElement("p", null, _react.default.createElement("strong", null, "Error:"), " ", error.toString()));
};

var Wrapper = function Wrapper(props) {
  return props.children;
};
/**
 * <p>This function is used to indicate whether the current focus setting
 * means that the Widget is selected.  The function takes the current focus
 * and should return true or false.</p>
 * <p>The function supplied should return <code>true</code> if the item is selected with the given focus.</p>
 * @callback SelectedFunction
 * @param {object} focus - the currently specified focus
 * @returns {boolean}
 * @example
 * list.map(item) => <Widget design={item} key={item.id}
 *    focusOn={{selectedItem: item, selectedItems: list}}
 *    isSelected={(focus)=>focus.selectedItem === item} />
 */

/**
 * <p>Widget components always have a design and a type, providing a specification
 * and a triggering hook point to inject functionality using the inversion
 * of control events object.</p>
 * <p>
 *     The principle is that each Widget will fire events and provide an api
 *     that the recipients of the event can use to populate part of the
 *     user interface with components that represent the contents and
 *     if desired, editors for that contents.
 * </p>
 * <p>Widget components will frequently contain other Widget components as you recurse
 * through your document's design to render all of it. You can use {@link SortableWidgetContainer} and {@link SortableWidget} to
 * represent lists of items and the individual items in a way that means the user can drag and
 * drop them to reorganise the list.</p>
 * @kind component
 * @classdesc The fundamental building block of the {@link Widgets} user interface. Each
 * Widget represents a place that functionality can be injected into the document and
 * the design.
 * @property {object} design
 * Specifies the object that this widget will
 * use to create its contents.  Normally this
 * will be a sub element of the current
 * <code>design</code> used to render the component
 * utilising the Widget<br>
 * <code>&lt;Widget design={design.questions}/&gt;</code>
 * @property {string|Array.<string>} type
 * A type or an array of types that will be used for this Widget.
 * This is how a widget finds the items to dynamically display
 * within it.
 * @property {object} [document]
 * The object that represents the document for
 * the subsection used by the widget. Normally this
 * will be a sub element of the current <code>document</code>. <br>
 * <code>&lt;Widget design={design.questions} document={document.answers}/&gt;</code>
 * @property {Any} [value]
 * For use with the useValue hook and the setValue property, this specifies what
 * the current value should be for the Widget.
 * <br>
 * This is handy if you need to map a primitive value from the `document` to the
 * Widget.
 * <br>
 * <code>
 *
 *      &lt;Widget
 *           design={item}
 *           value={document[item.id]}
 *           setValue={value=>{
 *              document[item.id] = value;
 *            }}
 *      /&gt;
 * </code>
 * @property {SetValueFunction} [setValue]
 * For use with the `useValue` hook and the `value` property, this specifies
 * how to set an updated value back into the document
 *
 * This is handy if you need to map a primitive value from the `document` to the
 * Widget.
 * @property {object} [parent]
 * Normally the parent of a widget is set automatically by
 * the system as the `design` that was in play when it was
 * created.  You can override this behaviour by specifying
 * a particular `parent`.
 * @property {SelectedFunction} [isSelected]
 * A function which indicates whether the current item should
 * be displayed as selected.  If omitted the Widget will
 * be considered selected if the current focus is exactly
 * equal to the `focusOn` prop if set, or the design otherwise.
 *
 * Frequently you will specify isSelected when you want
 * a parent item to display as selected as well as its children.
 * To do this you'll probably have both "in focus".
 * @property {object} [focusOn]
 * When this Widget is selected it will normally set the focus
 * to be the `design`, you can override that by specifying `focusOn`.
 *
 * See isSelected
 * @property {string} [endOfLineClass]
 * CSS class used for the endOfLine box - can be used to move
 * it to an absolute position etc based on where pop up
 * controls should appear.  This is normally used for editMode components
 * @property {string} [inLineClass]
 * CSS class used for the inline box (below) - can be used to move
 * it to an absolute position etc based on where pop up
 * controls should appear.  This is normally used for editMode components
 * @property {string} [className]
 * CSS class used for the content box of the widget, used for normal display.
 * @property {boolean} [requiresSelection]
 * Specifies that this widget and its children
 * should be completely redrawn when they are
 * focused or defocused.  This should be avoided
 * if at all possible
 */


var Widget = (0, _reactJss.default)(_widgetStyles.styles)(function Widget(_ref2) {
  var classes = _ref2.classes,
      requiresSelection = _ref2.requiresSelection,
      children = _ref2.children,
      design = _ref2.design,
      document = _ref2.document,
      parent = _ref2.parent,
      _ref2$className = _ref2.className,
      className = _ref2$className === void 0 ? "" : _ref2$className,
      _ref2$endOfLineClass = _ref2.endOfLineClass,
      endOfLineClass = _ref2$endOfLineClass === void 0 ? "" : _ref2$endOfLineClass,
      _ref2$type = _ref2.type,
      type = _ref2$type === void 0 ? "unknown" : _ref2$type,
      focusOn = _ref2.focusOn,
      isSelected = _ref2.isSelected,
      props = _objectWithoutProperties(_ref2, ["classes", "requiresSelection", "children", "design", "document", "parent", "className", "endOfLineClass", "type", "focusOn", "isSelected"]);

  var types = Array.isArray(type) ? type : [type];
  var context = (0, _contexts.useWidgetContext)();
  var widgets = context.widgets,
      editMode = context.editMode,
      onChanged = context.onChanged,
      focus = context.bag.focus,
      setFocus = context.setFocus;

  var _useDesign = (0, _contexts.useDesign)(design);

  var _useDesign2 = _slicedToArray(_useDesign, 1);

  design = _useDesign2[0];
  parent = parent || context.design;
  document = document || context.document;

  isSelected = isSelected || function (focus) {
    return (0, _isEqual.default)(focus, focusOn || design);
  };

  var inline = [];
  var endOfLine = [];
  var content = [];

  var widgetContext = _objectSpread({}, props, context, {
    editMode: editMode,
    design: design,
    parent: parent,
    document: document,
    _isSelected: isSelected,
    isSelected: requiresSelection ? (0, _contexts.useFocus)(isSelected)[0] : isSelected(focus),
    onChanged: onChanged,
    type: type,
    widgets: widgets,
    inline: inline,
    endOfLine: endOfLine,
    content: content
  });

  var eventContext = {
    inline: inline,
    endOfLine: endOfLine,
    content: content,
    design: design
  };
  if (children) content.push(function () {
    return _react.default.createElement(Wrapper, null, children);
  });

  _contexts.useWidgetContext.with(_objectSpread({}, widgetContext, {
    _type: "render"
  }), function () {
    types.forEach(function (type) {
      widgets.emit("render.".concat(type), eventContext);
    });
  });

  function processFocus(event) {
    var newFocus = focusOn || design;
    setFocus(newFocus);
    event.stopPropagation();
  }

  return _react.default.createElement("div", {
    onFocus: processFocus,
    onClick: processFocus,
    className: (0, _classnames.default)(classes.widgetContext, className)
  }, _react.default.createElement(_contexts.WidgetContext.Provider, {
    value: widgetContext
  }, _react.default.createElement(_reactErrorBoundary.ErrorBoundary, {
    FallbackComponent: ErrorReport
  }, _react.default.createElement(_selectionArea.SelectionArea, {
    visible: editMode
  }), _react.default.createElement(_reactstrap.Row, {
    className: classes.widgetRow
  }, _react.default.createElement(_panel.Panel, {
    list: content
  }), _react.default.createElement(_panel.Panel, {
    visible: editMode,
    list: endOfLine
  }, function (list) {
    return _react.default.createElement(_reactstrap.Col, {
      xs: "auto",
      className: "d-flex align-items-center ".concat(endOfLineClass)
    }, _react.default.createElement(_reactstrap.ButtonGroup, {
      size: "sm"
    }, list));
  })), _react.default.createElement(_swallowClicks.default, {
    style: {
      position: "relative",
      zIndex: 2
    }
  }, _react.default.createElement(_panel.Panel, {
    visible: editMode,
    list: inline
  })))));
});
exports.Widget = Widget;
var _default = Widget;
exports.default = _default;
Widget.propTypes = {
  requiresSelection: _propTypes.default.bool,
  design: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  document: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  parent: _propTypes.default.object,
  isSelected: _propTypes.default.func,
  focusOn: _propTypes.default.object,
  type: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.arrayOf(_propTypes.default.string)]).isRequired,
  endOfLineClass: _propTypes.default.string,
  inlineClass: _propTypes.default.string,
  className: _propTypes.default.string,
  value: _propTypes.default.any,
  setValue: _propTypes.default.func
};

/**
 * Use this component inside a {@link SortableWidgetContainer} to allow for user initiated drag
 * and drop of items
 * @kind component
 * @classdesc A drag and drop sortable version of {@link Widget} to be used as each item within a sortable list
 * @augments {Widget}
 * @property {number} index - the list index number inside the sorted list
 * @property {boolean} [disabled] - if disabled the item cannot be dragged
 */
var SortableWidget = (0, _reactSortableHoc.SortableElement)(Widget);
exports.SortableWidget = SortableWidget;
SortableWidget.propTypes = {
  requiresSelection: _propTypes.default.bool,
  design: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  document: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  parent: _propTypes.default.object,
  isSelected: _propTypes.default.func,
  focusOn: _propTypes.default.object,
  type: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.arrayOf(_propTypes.default.string)]),
  endOfLineClass: _propTypes.default.string,
  inlineClass: _propTypes.default.string,
  className: _propTypes.default.string,
  value: _propTypes.default.any,
  setValue: _propTypes.default.func,
  index: _propTypes.default.number.isRequired,
  disabled: _propTypes.default.bool
  /**
   * Called when sorting on a sortable list is over. Often this will just
   * pass on to {@link arrayMoveInPlace} which will move the item in
   * a standard array.  You will also need to call an `update` method
   * (probably retrieved from {@link useDesign}) to say that the list
   * has been changed afterwards.
   * @callback OnSortEnd
   * @param {number} oldIndex - the index of the item that was moved
   * @param {number} newIndex - the index of where the item was dropped
   */

  /**
   * Put {@link SortableWidget}s inside this component to create a list that can be
   * arranged by user initiated drag and drop
   * @kind component
   * @classdesc A Widget that represents the list which will contain sortable items.
   * @augments {Widget}
   * @type {React.ComponentClass<any>}
   * @property {OnSortEnd} onSortEnd
   * A function that is called and passed the indices of
   * the item to move.
   * @property {number} [distance]
   * The amount of distance the pointer should be
   * dragged before starting a sort.  To avoid issues
   * with single clicks this should be more than 0
   * @property {Axis} [axis]
   * Which axes you have your widgets laid out on,
   * simple lists should just use <code>y</code> while grids
   * and dashboards normally require <code>xy</code>
   */

};
var SortableWidgetContainer = (0, _reactSortableHoc.SortableContainer)(Widget);
exports.SortableWidgetContainer = SortableWidgetContainer;
SortableWidgetContainer.propTypes = {
  requiresSelection: _propTypes.default.bool,
  design: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  document: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  parent: _propTypes.default.object,
  isSelected: _propTypes.default.func,
  focusOn: _propTypes.default.object,
  type: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.arrayOf(_propTypes.default.string)]),
  endOfLineClass: _propTypes.default.string,
  inlineClass: _propTypes.default.string,
  className: _propTypes.default.string,
  value: _propTypes.default.any,
  setValue: _propTypes.default.func,
  onSortEnd: _propTypes.default.func.isRequired,
  distance: _propTypes.default.number,
  axis: _propTypes.default.oneOf(['y', 'xy'])
};