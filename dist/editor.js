"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Editor = exports.EditorFrame = void 0;

var _reactJss = _interopRequireDefault(require("react-jss"));

var _contexts = require("./contexts");

var _react = _interopRequireWildcard(require("react"));

var _fi = require("react-icons/fi");

var _reactstrap = require("reactstrap");

var _classnames = _interopRequireDefault(require("classnames"));

var _utilities = require("./utilities");

var _widgetStyles = require("./widget-styles");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _events = require("./events");

var _typeExtractor = _interopRequireDefault(require("./type-extractor"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * Used to send a message upwards to set a value
 * @callback SetValueFunction
 * @param {boolean} value - the value to set
 */

/**
 * @classdesc The EditorFrame provides an automatic popup {@link Editor} when placed inside a
 * [Widgets]{@link Widgets} component.
 * @kind component
 * @property {boolean} [editMode] - Whether the editor is in edit mode
 * @property {SetValueFunction} [setEditMode]
 * A function that can be used to set the current edit mode
 * @property {number[]} [editorMinHeight]
 * Array of numbers that are used in combination with the available height
 * to specify how large the editor should be at minimum.
 *
 * Numbers < 1 represent a proportion of the height, numbers greater than 1
 * indicate a height in pixels. Normally you will use 3 values.
 *     [idealProportion, minimumPixelHeight, noMoreThanThisProportion]
 * e.g.
 *     [0.3, 400, 0.7]
 * Ideally 30% of the height, but a minimum of 400 pixels, but no more than 70% of the height
 * @property {number[]} [editorMaxHeight]
 * Array of numbers that are used in combination with the available height
 * to specify how large the editor should be at maximum
 *
 * Numbers < 1 represent a proportion of the height, numbers greater than 1
 * indicate a height in pixels. Normally you will use 3 values.
 * @property {number} [height]
 * The height of the available space for the editor
 * @property {object} [context]
 * Context object to specify additional editor parameters
 * @property {OnUpdated} [onUpdated]
 * Called any time the design changes due to editor actions
 * @example
 * return <Widgets design={someDesign}>
 *     <EditorFrame/>
 * </Widgets>
 * @see {@link Editor} for a standalone editor
 */
var EditorFrame = (0, _reactJss.default)(_widgetStyles.styles)(function EditorFrame(_ref) {
  var _ref$context = _ref.context,
      context = _ref$context === void 0 ? {} : _ref$context,
      props = _objectWithoutProperties(_ref, ["context"]);

  context = _objectSpread({}, (0, _contexts.useWidgetContext)(), props, context);
  var _context = context,
      editMode = _context.editMode,
      setEditMode = _context.setEditMode,
      classes = _context.classes;
  return !editMode ? _react.default.createElement("div", {
    "data-testid": "enteredit",
    className: classes.editorButton,
    onClick: function onClick() {
      return setEditMode(true);
    }
  }, _react.default.createElement(_fi.FiSettings, null)) : _react.default.createElement("div", {
    className: classes.editor
  }, _react.default.createElement("div", {
    "data-testid": "exitedit",
    onClick: function onClick() {
      return setEditMode(false);
    },
    className: classes.activeEditorButton
  }, _react.default.createElement(_fi.FiX, null)), _react.default.createElement(Editor, context));
});
exports.EditorFrame = EditorFrame;
EditorFrame.propTypes = {
  editMode: _propTypes.default.bool,
  setEditMode: _propTypes.default.func,
  editorMinHeight: _propTypes.default.arrayOf(_propTypes.default.number),
  editorMaxHeight: _propTypes.default.arrayOf(_propTypes.default.number),
  height: _propTypes.default.number,
  context: _propTypes.default.object,
  onUpdated: _propTypes.default.func
  /**
   * @kind component
   * @classdesc A standalone editor that can be bound to a {@link Widgets} component. {@link EditorFrame} provides
   * a quicker way of embedding an editor into a {@link Widgets} component.
   * @property {Object} bag - an object that is used to share data between editors and the display component, if you are
   * using an Editor then you should set the same bag on the {@link Widgets}
   * @property {string} id - an identifier for this Editor component, which should be matched with a similar id on the
   * {@link Widgets}
   * @property {Object} design - the design of the document that will be edited by this Editor
   * @property {Object} [document] - the document contents being edited
   * @property {string} [type] - the type of the document being edited, if this isn't specified the system tries to
   * extract it from the design or the document itself and provides an event for plugins to override or provide mapping
   * @property {boolean} [editMode] - Whether the editor is in edit mode
   * @property {SetValueFunction} [setEditMode]
   * A function that can be used to set the current edit mode
   * @property {number[]} [editorMinHeight]
   * Array of numbers that are used in combination with the available height
   * to specify how large the editor should be at minimum.
   *
   * Numbers < 1 represent a proportion of the height, numbers greater than 1
   * indicate a height in pixels. Normally you will use 3 values.
   *     [idealProportion, minimumPixelHeight, noMoreThanThisProportion]
   * e.g.
   *     [0.3, 400, 0.7]
   * Ideally 30% of the height, but a minimum of 400 pixels, but no more than 70% of the height
   * @property {number[]} [editorMaxHeight]
   * Array of numbers that are used in combination with the available height
   * to specify how large the editor should be at maximum
   *
   * Numbers < 1 represent a proportion of the height, numbers greater than 1
   * indicate a height in pixels. Normally you will use 3 values.
   * @property {number} [height]
   * The height of the available space for the editor
   * @property {object} [context]
   * Context object to specify additional editor parameters
   * @property {OnUpdated} [onUpdated]
   * Called any time the design changes due to editor actions
   * @property {WidgetEvents} [widgets] - an event source to use, useful for testing, defaults to the globalWidgets event source
   * @example
   * let bag = {}
   * return <div>
   *     <Widgets design={someDesign} id="example" bag={bag} type="test"/>
   *     <Editor design={someDesign} id="example" bag={bag} type="test"/>
   * </div>
   * @see {@link EditorFrame} for an easy to use plug in editor for {@link Widgets}
   */

};
var Editor = (0, _reactJss.default)(_widgetStyles.styles)(function Editor(_ref2) {
  var classes = _ref2.classes,
      style = _ref2.style,
      _ref2$editorMinHeight = _ref2.editorMinHeight,
      editorMinHeight = _ref2$editorMinHeight === void 0 ? [0.3, 300, 0.7] : _ref2$editorMinHeight,
      _ref2$onUpdated = _ref2.onUpdated,
      onUpdated = _ref2$onUpdated === void 0 ? _utilities.noop : _ref2$onUpdated,
      _ref2$editorMaxHeight = _ref2.editorMaxHeight,
      editorMaxHeight = _ref2$editorMaxHeight === void 0 ? [0.4, 400, 0.6] : _ref2$editorMaxHeight,
      _ref2$height = _ref2.height,
      height = _ref2$height === void 0 ? window.innerHeight : _ref2$height,
      context = _objectWithoutProperties(_ref2, ["classes", "style", "editorMinHeight", "onUpdated", "editorMaxHeight", "height"]);

  context = _objectSpread({}, (0, _contexts.useWidgetContext)(), context.context, context);

  if (!context.bag || !context.design || !context.id) {
    return _react.default.createElement(_reactstrap.Alert, {
      className: classes.alert,
      color: "danger"
    }, "Editor component not linked to a context. You should provide:", _react.default.createElement("ul", null, _react.default.createElement("li", null, _react.default.createElement("strong", null, "id"), " - an identifier shared with the component displaying the design"), _react.default.createElement("li", null, _react.default.createElement("strong", null, "bag"), " - an object to contain shared data between the editor and display, should be set to the same value for both"), _react.default.createElement("li", null, _react.default.createElement("strong", null, "design"), " - the document being edited"), _react.default.createElement("li", null, _react.default.createElement("strong", null, _react.default.createElement("em", null, "type")), " - either a specified value ", _react.default.createElement("strong", null, "or"), " the type of the document must be discovered as being the same as for the display component when the design is processed")));
  }

  var editor = {
    header: [],
    tabs: {
      general: {
        title: _react.default.createElement(_fi.FiSettings, null),
        content: []
      }
    }
  };
  var _context2 = context,
      _context2$widgets = _context2.widgets,
      widgets = _context2$widgets === void 0 ? _events.globalWidgets : _context2$widgets,
      documentType = _context2.documentType,
      id = _context2.id,
      _context2$onChanged = _context2.onChanged,
      onChanged = _context2$onChanged === void 0 ? onUpdated : _context2$onChanged,
      type = _context2.type,
      design = _context2.design,
      bag = _context2.bag,
      document = _context2.document;
  documentType = documentType || (0, _typeExtractor.default)({
    type: type
  }, document, design);
  bag.focus = bag.focus || {};
  var minHeight = height;
  var maxHeight = height;
  /* Calculate the editor height */

  editorMinHeight = Array.isArray(editorMinHeight) ? editorMinHeight : [editorMinHeight];
  editorMaxHeight = Array.isArray(editorMaxHeight) ? editorMaxHeight : [editorMaxHeight];
  editorMinHeight.forEach(function (h) {
    return h < 1 ? minHeight = Math.min(minHeight, height * h) : minHeight = Math.max(minHeight, h);
  });
  editorMaxHeight.forEach(function (h) {
    return h < 1 ? maxHeight = Math.min(maxHeight, height * h) : maxHeight = Math.max(maxHeight, h);
  }); //Handle updates to the document

  (0, _react.useEffect)(function () {
    widgets.on("update.".concat(documentType, ".").concat(id, ".*"), onChanged);
    return function cleanup() {
      widgets.removeListener("update.".concat(documentType, ".").concat(id, ".*"), onChanged);
    };
  });

  var _useState = (0, _react.useState)("general"),
      _useState2 = _slicedToArray(_useState, 2),
      currentTab = _useState2[0],
      setTab = _useState2[1];

  editor.tabs = {
    general: {
      title: _react.default.createElement(_fi.FiSettings, null),
      content: []
    }
  };
  context.documentType = documentType;
  context.widgets = widgets;
  context.editor = editor;
  _contexts.useWidgetContext.override = _objectSpread({}, context, {
    _type: "editor"
  });

  var _useFocus = (0, _contexts.useFocus)(),
      _useFocus2 = _slicedToArray(_useFocus, 2),
      focus = _useFocus2[1];

  context.focus = focus;
  widgets.emit("editor.".concat(documentType), context);

  if (!editor.tabs[currentTab]) {
    currentTab = "general";
  }

  var result = _react.default.createElement(_contexts.WidgetContext.Provider, {
    value: context
  }, _react.default.createElement("div", {
    onClick: function onClick(event) {
      return event.stopPropagation();
    }
  }, _react.default.createElement("div", {
    className: classes.editorInner
  }, _react.default.createElement(_reactstrap.Nav, {
    tabs: true
  }, Object.entries(editor.tabs).sort(_utilities.inPriorityOrder).map(function (_ref3) {
    var _ref4 = _slicedToArray(_ref3, 2),
        key = _ref4[0],
        tab = _ref4[1];

    return _react.default.createElement(_reactstrap.NavItem, {
      className: classes.tab,
      key: key
    }, _react.default.createElement(_reactstrap.NavLink, {
      className: (0, _classnames.default)({
        active: currentTab === key
      }),
      onClick: function onClick() {
        return setTab(key);
      }
    }, tab.title));
  })), _react.default.createElement(_reactstrap.TabContent, {
    style: _objectSpread({
      maxHeight: maxHeight,
      minHeight: minHeight
    }, style),
    className: classes.editorContent,
    activeTab: currentTab
  }, Object.entries(editor.tabs).map(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 2),
        key = _ref6[0],
        tab = _ref6[1];

    var children = (0, _utilities.processRenderFunctions)(tab.content, context).map(function (Child, key) {
      return _react.default.createElement(Child, {
        key: key,
        tab: tab
      });
    });
    return _react.default.createElement(_reactstrap.TabPane, {
      key: key,
      tabId: key
    }, _react.default.createElement(_reactstrap.Container, null, _react.default.createElement("br", null), children));
  })))));

  _contexts.useWidgetContext.override = null;
  return result;
});
exports.Editor = Editor;
Editor.propTypes = {
  id: _propTypes.default.string.isRequired,
  design: _propTypes.default.object.isRequired,
  bag: _propTypes.default.object.isRequired,
  type: _propTypes.default.string,
  editMode: _propTypes.default.bool,
  setEditMode: _propTypes.default.func,
  editorMinHeight: _propTypes.default.arrayOf(_propTypes.default.number),
  editorMaxHeight: _propTypes.default.arrayOf(_propTypes.default.number),
  height: _propTypes.default.number,
  context: _propTypes.default.object,
  onUpdated: _propTypes.default.func
};