"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactTestingLibrary = require("react-testing-library");

var _swallowClicks = _interopRequireDefault(require("./swallow-clicks"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

it("Should swallow clicks",
/*#__PURE__*/
_asyncToGenerator(
/*#__PURE__*/
regeneratorRuntime.mark(function _callee() {
  var callBack, _render, getByTestId;

  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          callBack = jest.fn();
          _render = (0, _reactTestingLibrary.render)(_react.default.createElement("div", {
            onClick: callBack
          }, _react.default.createElement(_swallowClicks.default, null, _react.default.createElement("button", {
            "data-testid": "button"
          }, "button")))), getByTestId = _render.getByTestId;

          _reactTestingLibrary.fireEvent.click(getByTestId("button"));

          expect(callBack).toHaveBeenCalledTimes(0);

        case 4:
        case "end":
          return _context.stop();
      }
    }
  }, _callee, this);
})));
it("Should swallow focus",
/*#__PURE__*/
_asyncToGenerator(
/*#__PURE__*/
regeneratorRuntime.mark(function _callee2() {
  var callBack, _render2, getByTestId;

  return regeneratorRuntime.wrap(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          callBack = jest.fn();
          _render2 = (0, _reactTestingLibrary.render)(_react.default.createElement("div", {
            onFocus: callBack
          }, _react.default.createElement(_swallowClicks.default, null, _react.default.createElement("input", {
            "data-testid": "button"
          })))), getByTestId = _render2.getByTestId;

          _reactTestingLibrary.fireEvent.focus(getByTestId("button"));

          expect(callBack).toHaveBeenCalledTimes(0);

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  }, _callee2, this);
})));