"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactTestingLibrary = require("react-testing-library");

var _widgets = require("./widgets");

var _events = require("./events");

var _contexts = require("./contexts");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var widgets = null;
beforeEach(function () {
  widgets = (0, _events.widgetEventsFactory)();
});
it("should insert multiple items", function () {
  var One = function One() {
    return _react.default.createElement("div", null, "1");
  };

  var Two = function Two() {
    return _react.default.createElement("div", null, "2");
  };

  var design = {
    type: "test"
  },
      document = {};
  widgets.configure("test", function () {
    (0, _contexts.useLayout)({
      content: [One]
    });
  });
  widgets.configure("test", function () {
    (0, _contexts.useLayout)({
      content: [Two]
    });
  });

  var _render = (0, _reactTestingLibrary.render)(_react.default.createElement(_widgets.Widgets, {
    widgets: widgets,
    document: document,
    design: design
  })),
      getByText = _render.getByText;

  expect(getByText("1"));
  expect(getByText("2"));
});
it("should allow a function to modify the list", function () {
  var One = function One() {
    return _react.default.createElement("div", null, "1");
  };

  var Two = function Two() {
    return _react.default.createElement("div", null, "2");
  };

  Two.onBeforeRender = function (list) {
    return list.length = list.length - 1;
  };

  var design = {
    type: "test"
  },
      document = {};
  widgets.configure("test", function () {
    (0, _contexts.useLayout)({
      content: [One]
    });
  });
  widgets.configure("test", function () {
    (0, _contexts.useLayout)({
      content: [Two]
    });
  });

  var _render2 = (0, _reactTestingLibrary.render)(_react.default.createElement(_widgets.Widgets, {
    widgets: widgets,
    document: document,
    design: design
  })),
      getByText = _render2.getByText,
      queryByText = _render2.queryByText;

  expect(queryByText("1")).not.toBe(null);
  expect(queryByText("2")).toBe(null);
});
it("should support a modified array return value", function () {
  var One = function One() {
    return _react.default.createElement("div", null, "1");
  };

  var Two = function Two() {
    return _react.default.createElement("div", null, "2");
  };

  Two.onBeforeRender = function () {
    return [Two];
  };

  var design = {
    type: "test"
  },
      document = {};
  widgets.configure("test", function () {
    (0, _contexts.useLayout)({
      content: [One]
    });
  });
  widgets.configure("test", function () {
    (0, _contexts.useLayout)({
      content: [Two]
    });
  });

  var _render3 = (0, _reactTestingLibrary.render)(_react.default.createElement(_widgets.Widgets, {
    widgets: widgets,
    document: document,
    design: design
  })),
      queryByText = _render3.queryByText;

  expect(queryByText("1")).toBe(null);
  expect(queryByText("2")).not.toBe(null);
});