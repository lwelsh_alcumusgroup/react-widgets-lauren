"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arrayMoveInPlace = arrayMoveInPlace;
Object.defineProperty(exports, "Widgets", {
  enumerable: true,
  get: function get() {
    return _widgets.Widgets;
  }
});
Object.defineProperty(exports, "inPriorityOrder", {
  enumerable: true,
  get: function get() {
    return _utilities.inPriorityOrder;
  }
});
Object.defineProperty(exports, "useAsync", {
  enumerable: true,
  get: function get() {
    return _useAsync.useAsync;
  }
});
Object.defineProperty(exports, "useBag", {
  enumerable: true,
  get: function get() {
    return _contexts.useBag;
  }
});
Object.defineProperty(exports, "useContent", {
  enumerable: true,
  get: function get() {
    return _contexts.useContent;
  }
});
Object.defineProperty(exports, "useContextMenu", {
  enumerable: true,
  get: function get() {
    return _contexts.useContextMenu;
  }
});
Object.defineProperty(exports, "useDesign", {
  enumerable: true,
  get: function get() {
    return _contexts.useDesign;
  }
});
Object.defineProperty(exports, "useDocument", {
  enumerable: true,
  get: function get() {
    return _contexts.useDocument;
  }
});
Object.defineProperty(exports, "useEditMode", {
  enumerable: true,
  get: function get() {
    return _contexts.useEditMode;
  }
});
Object.defineProperty(exports, "useEndOfLine", {
  enumerable: true,
  get: function get() {
    return _contexts.useEndOfLine;
  }
});
Object.defineProperty(exports, "useEvents", {
  enumerable: true,
  get: function get() {
    return _contexts.useEvents;
  }
});
Object.defineProperty(exports, "useFocus", {
  enumerable: true,
  get: function get() {
    return _contexts.useFocus;
  }
});
Object.defineProperty(exports, "useFocused", {
  enumerable: true,
  get: function get() {
    return _contexts.useFocused;
  }
});
Object.defineProperty(exports, "useInline", {
  enumerable: true,
  get: function get() {
    return _contexts.useInline;
  }
});
Object.defineProperty(exports, "useLayout", {
  enumerable: true,
  get: function get() {
    return _contexts.useLayout;
  }
});
Object.defineProperty(exports, "useParent", {
  enumerable: true,
  get: function get() {
    return _contexts.useParent;
  }
});
Object.defineProperty(exports, "useRoot", {
  enumerable: true,
  get: function get() {
    return _contexts.useRoot;
  }
});
Object.defineProperty(exports, "useTabs", {
  enumerable: true,
  get: function get() {
    return _contexts.useTabs;
  }
});
Object.defineProperty(exports, "useValue", {
  enumerable: true,
  get: function get() {
    return _contexts.useValue;
  }
});
Object.defineProperty(exports, "useWidgetContext", {
  enumerable: true,
  get: function get() {
    return _contexts.useWidgetContext;
  }
});
Object.defineProperty(exports, "Widget", {
  enumerable: true,
  get: function get() {
    return _widgetComponent.Widget;
  }
});
Object.defineProperty(exports, "SortableWidget", {
  enumerable: true,
  get: function get() {
    return _widgetComponent.SortableWidget;
  }
});
Object.defineProperty(exports, "SortableWidgetContainer", {
  enumerable: true,
  get: function get() {
    return _widgetComponent.SortableWidgetContainer;
  }
});
Object.defineProperty(exports, "globalWidgets", {
  enumerable: true,
  get: function get() {
    return _events.globalWidgets;
  }
});
Object.defineProperty(exports, "widgetEventsFactory", {
  enumerable: true,
  get: function get() {
    return _events.widgetEventsFactory;
  }
});
Object.defineProperty(exports, "Editor", {
  enumerable: true,
  get: function get() {
    return _editor.Editor;
  }
});
Object.defineProperty(exports, "EditorFrame", {
  enumerable: true,
  get: function get() {
    return _editor.EditorFrame;
  }
});
exports.default = void 0;

var _widgets = require("./widgets");

var _utilities = require("./utilities");

var _useAsync = require("./use-async");

var _contexts = require("./contexts");

var _widgetComponent = require("./widget-component");

var _events = require("./events");

var _editor = require("./editor");

/**
 * Helper function to rearrange an array, useful when used with {@link SortableWidgetContainer} in the onSortEnd method
 * @param {Array} array - the array to modify
 * @param {number} previousIndex - the previous index
 * @param {number} newIndex - the new index of the item
 * @example
 return <SortableWidgetContainer {...props}
 isSelected={focus => focus.selectedQuestions === questions}
 axis="y"
 distance={4}
 focusOn={{selectedQuestions: questions}}
 design={questions}
 onSortEnd={({oldIndex, newIndex}) => {
                                            arrayMoveInPlace(questions, oldIndex, newIndex)
                                            update()
                                        }}
 document={answers}
 type="questions">
 {items}
 </SortableWidgetContainer>
 */
function arrayMoveInPlace(array, previousIndex, newIndex) {
  if (newIndex >= array.length) {
    var k = newIndex - array.length;

    while (k-- + 1) {
      array.push(undefined);
    }
  }

  array.splice(newIndex, 0, array.splice(previousIndex, 1)[0]);
}

var _default = _widgets.Widgets;
exports.default = _default;