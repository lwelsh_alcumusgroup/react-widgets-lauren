"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useFocused = useFocused;
exports.useInline = useInline;
exports.useLayout = useLayout;
exports.useContent = useContent;
exports.useEndOfLine = useEndOfLine;
exports.useEvents = useEvents;
exports.useTabs = useTabs;
exports.useWidgetContext = useWidgetContext;
exports.useFocus = useFocus;
exports.useDesign = useDesign;
exports.useBag = useBag;
exports.useDocument = useDocument;
exports.useValue = useValue;
exports.useParent = useParent;
exports.useEditMode = useEditMode;
exports.useRoot = useRoot;
exports.useContextMenu = useContextMenu;
exports.EditorContext = exports.WidgetContext = void 0;

var _react = _interopRequireWildcard(require("react"));

var _shortid = _interopRequireDefault(require("shortid"));

var _isObject = _interopRequireDefault(require("lodash/isObject"));

var _isFunction = _interopRequireDefault(require("lodash/isFunction"));

var _merge = _interopRequireDefault(require("lodash/merge"));

var _mergeWith = _interopRequireDefault(require("lodash/mergeWith"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Any React component
 * @typedef {function|Class} ReactComponent
 */

/**
 * Adds a ReactComponent to the list to be rendered
 * @callback addItem
 * @param {ReactComponent} component - the component to add
 */

/**
 * Return from a useLayout
 * @typedef ComponentListReturn
 * @type {array}
 * @property {ReactComponent[]} 0 - the current components
 * @property {addItem} 1 - a function to add a new item to the list
 */

/**
 * A description of the layout canvas of a Widgets framework
 * @typedef {Object} Layout
 * @property {ReactComponent[]} headerLeft - Top left components
 * @property {ReactComponent[]} headerCentre - Header centre components
 * @property {ReactComponent[]} headerRight - Top right components
 * @property {ReactComponent[]} left - Left panel components
 * @property {ReactComponent[]} content - Core {@link Widgets} contents
 * @property {ReactComponent[]} right - Right panel components
 * @property {ReactComponent[]} footer - Footer components
 */

/**
 * Sets the layout by merging a layout definition into the existing
 * layout
 * @callback setLayout
 * @param {Layout} layout - the layout to merge in
 */

/**
 * Return from a useLayout
 * @typedef LayoutReturn
 * @type {array}
 * @property {Layout} 0 - the current layout
 * @property {setLayout} 1 - a function to merge in a new layout
 */
var WidgetContext = _react.default.createContext({
  design: null,
  document: null
});

exports.WidgetContext = WidgetContext;

var EditorContext = _react.default.createContext(null);

exports.EditorContext = EditorContext;

function noop() {}

function useFocus() {
  var isSelected = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : noop;

  if (!(0, _isFunction.default)(isSelected)) {
    throw new Error("Wrong parameter to useFocus, did you mean useFocused?");
  }

  var _useWidgetContext = useWidgetContext(),
      widgets = _useWidgetContext.widgets,
      id = _useWidgetContext.id,
      documentType = _useWidgetContext.documentType,
      bag = _useWidgetContext.bag;

  var focus = bag ? bag.focus : {};

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      setSelected = _useState2[1];

  var currentSelectedState = isSelected(focus);

  function focusChange(focus) {
    var selected = isSelected(focus);

    if (currentSelectedState === undefined || selected !== currentSelectedState) {
      currentSelectedState = selected;
      setSelected(selected);
    }
  }

  (0, _react.useEffect)(function () {
    widgets.addListener("focus.".concat(documentType, ".").concat(id), focusChange);
    return function cleanup() {
      widgets.removeListener("focus.".concat(documentType, ".").concat(id), focusChange);
    };
  });
  return [currentSelectedState, focus];
}

var DUMMY = {};
/**
 * Return the part of the current focus that should be used
 * @callback FocusSelection
 * @param {object} focus - the current focus of the {@link Widgets}
 * @returns {object}
 */

/**
 * <p>
 *     Used a lot within [editor]{@link WidgetEvents#editor} events, you use this
 *     hook to get a currently focused item.
 * </p>
 * @param {FocusSelection|string} selector - the item that should be selected, either a function that takes
 * the current focus and returns the item or a string specifying the property of the focus to use
 * @returns {UseDesignReturn|Array}
 * @example
 widgets.editor("dashboard", () => {
        const [selectedWidget] = useFocused("selectedWidget")
        if (selectedWidget && selectedWidget.type === "number") {
            useTabs(DataEditor, "Data")
        }
    })

 */

function useFocused(selector) {
  var _useFocus = useFocus(),
      _useFocus2 = _slicedToArray(_useFocus, 2),
      focus = _useFocus2[1];

  var value = (0, _isFunction.default)(selector) ? selector(focus) : selector ? focus[selector] : focus;

  if (!value) {
    useDesign(DUMMY);
    return [null, noop];
  } else {
    return useDesign(value);
  }
}

/**
 * The MergeUpdate method takes a new object describing changed
 * properties and merges it into the existing state, firing
 * an update event.  Arrays in the merge will be replaced
 * @callback MergeUpdate
 * @param {object} [value] - merge the passed value into the existing value
 */

/**
 * @typedef UseDesignReturn
 * @type {array}
 * @property {object} 0 - The current document
 * @property {MergeUpdate} 1 - Called to merge updates into the design
 */

/**
 * <p>Provides access to the current <code>design</code> which normally
 * represents the structure of the document for the current recursive
 * point during the rendering.</p>
 * <p>See also {@link useDocument} for user entered values.</p>
 * @param {object} [design] - optional override for the currently in scope design
 * @returns {UseDesignReturn|Array}
 */
function useDesign() {
  var design = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : DUMMY;
  var context = useWidgetContext();

  var _useState3 = (0, _react.useState)(),
      _useState4 = _slicedToArray(_useState3, 2),
      setDesignReact = _useState4[1];

  design = design === DUMMY ? context.design : design || DUMMY;
  design.id = design.id || _shortid.default.generate();
  var id = context.id,
      documentType = context.documentType;
  var designId = design.id;

  function willUpdate() {
    setDesignReact();
  } //Ensure this component refreshes if something else edits the design


  (0, _react.useEffect)(function () {
    context.widgets.on("update.".concat(documentType, ".").concat(id, ".").concat(designId), willUpdate);
    return function cleanup() {
      context.widgets.removeListener("update.".concat(documentType, ".").concat(id, ".").concat(designId), willUpdate);
    };
  });
  return [design === DUMMY ? null : design, function setDesign() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    if ((0, _isObject.default)(value)) {
      (0, _merge.default)(design, value);
    }

    context.widgets.emit("update.".concat(documentType, ".").concat(id, ".").concat(designId), design);
  }, context];
}
/**
 * @typedef UseBagReturn
 * @type {array}
 * @property {object} 0 - The bag for the {@link Widgets} component
 * @property {MergeUpdate} 1 - A function to merge updates into the bag and redraw
 */

/**
 * Provides access to the "Bag" which is a shared data object that the UI can use to
 * keep track of values and communicate between components.
 * @returns {UseBagReturn|Array}
 * @example
 const FilterPanel = injectSheet(styles)(function FilterPanel({classes}) {
    const [bag, updateBag] = useBag()
    return <Col className={classes.column}>
        <FormGroup>
            <Label>Filter</Label>
            <Input type="text" value={bag.searchTerm || ""} onChange={(e) => {
                updateBag({searchTerm: e.target.value})
            }} placeholder="Filter Questions..."/>
        </FormGroup>
    </Col>
})
 FilterPanel.priority = 0
 */


function useBag() {
  var _useDesign = useDesign(),
      _useDesign2 = _slicedToArray(_useDesign, 3),
      update = _useDesign2[1],
      _useDesign2$ = _useDesign2[2],
      bag = _useDesign2$.bag,
      widgets = _useDesign2$.widgets,
      id = _useDesign2$.id,
      documentType = _useDesign2$.documentType;

  return [bag, function (value) {
    if ((0, _isObject.default)(value)) {
      (0, _merge.default)(bag, value);
    }

    widgets.emit("updatebag.".concat(documentType, ".").concat(id), bag);
    update();
  }];
}
/**
 * @typedef UseParentReturn
 * @type {array}
 * @property {object} 0 - The parent of the current design
 * @property {function} 1 - A function that will cause the parent and its children to redraw
 */

/**
 * Provides access to the parent of the current design while rendering.  This is handy for
 * redrawing lists if the order of items change or items are added or deleted
 * @returns {UseParentReturn|Array}
 */


function useParent() {
  var context = useWidgetContext();
  return [context.parent, function () {
    context.widgets.emit("update.".concat(context.documentType, ".").concat(context.id, ".").concat(context.parent.id), context.parent);
  }, context];
}
/**
 * @typedef UseDocumentReturn
 * @type {array}
 * @property {object} 0 - The current document
 * @property {MergeUpdate} 1 - Called to merge updates into the document

 */

/**
 * <p>Provides access to the current <code>document</code> which normally
 * represents the data entered by the user for the current recursive
 * point during the rendering.</p>
 * <p>See also {@link useValue} when representing recursed values in rendering components.</p>
 * @param {object} [document] - optional override for the currently in scope document
 * @returns {UseDocumentReturn|Array}
 */


function useDocument(document) {
  var context = useWidgetContext();

  var _useState5 = (0, _react.useState)(),
      _useState6 = _slicedToArray(_useState5, 2),
      setDocumentReact = _useState6[1];

  var design = context.design,
      id = context.id,
      documentType = context.documentType;
  document = document || context.document;
  design.id = design.id || _shortid.default.generate();

  function willUpdate() {
    setDocumentReact();
  } //Ensure this component refreshes if something else edits the design


  (0, _react.useEffect)(function () {
    context.widgets.on("updatevalue.".concat(documentType, ".").concat(id, ".").concat(design.id), willUpdate);
    return function cleanup() {
      context.widgets.removeListener("updatevalue.".concat(documentType, ".").concat(id, ".").concat(design.id), willUpdate);
    };
  });
  return [document, function setDocument() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    if ((0, _isObject.default)(value)) {
      (0, _merge.default)(document, value);
    }

    context.widgets.emit("updatevalue.".concat(documentType, ".").concat(id, ".").concat(design.id), document);
  }, context];
}
/**
 * @callback SetValueFromEventOrParamFunction
 * @param {any|Event} value - sets the value either to the contents of <code>event.target.value</code> if passed an
 * event, or the actual value passed in otherwise
 */

/**
 * @typedef UseValueReturn
 * @type {array}
 * @property {any} 0 - The current value
 * @property {SetValueFromEventOrParamFunction} 1 - The function to set the value, can take a value or an event

 */

/**
 * <p>
 *     Works with the {@link Widget} component's ability to specify <code>value</code> and <code>setValue</code>
 *     props which have a widget representing some primitive value.  The useValue hook lets you access and update
 *     the value from a renderer.
 * </p>
 * <p>
 *     The setValue function is capable of taking a primitive value of an event from an input.
 * </p>
 * @returns {UseValueReturn|Array}
 * @example
 const Render = injectSheet(style)(({classes}) => {
    const [value, setValue] = useValue()
    const [design] = useDesign()
    const {readOnly} = useWidgetContext()
    return (
        design.question ? <FormGroup key={design.id}>
                <Label>{design.question}</Label>
                <Input readOnly={readOnly} key={design.id} type="text" value={value || ""}
                       onChange={setValue} placeholder={design.placeholder || ""}/>
            </FormGroup>
            : (
                <Input readOnly={readOnly} key={design.id} className={classes.input} type="text" value={value || ""}
                       onChange={setValue} placeholder={design.placeholder || ""}/>
            )
    )
})

 */


function useValue() {
  var context = useWidgetContext();
  var value = context.value,
      setValue = context.setValue;

  var _useState7 = (0, _react.useState)(value),
      _useState8 = _slicedToArray(_useState7, 2),
      useValue = _useState8[0],
      setUseValue = _useState8[1];

  if (!setValue) {
    throw new Error("Unable to use value on a context that does not contain a setValue method");
  }

  var _useDocument = useDocument(),
      _useDocument2 = _slicedToArray(_useDocument, 2),
      update = _useDocument2[1];

  return [useValue, function (value) {
    if (value.target) {
      value = value.target.value;
    }

    if (useValue !== value) {
      setValue(value);
      setUseValue(value);
      update();
    }
  }];
}
/**
 * Returns the current widget context, which contains every parameter passed to the {@link Widgets} component.
 * So can be handy for accessing important external functions or values.
 * @returns {Object}
 * @example
 *     const {readOnly} = useWidgetContext()
 */


function useWidgetContext() {
  return useWidgetContext.override || (0, _react.useContext)(WidgetContext);
}

useWidgetContext.with = function (context, fn) {
  useWidgetContext.override = context;
  fn();
  useWidgetContext.override = null;
};
/**
 * Return from a useLayout
 * @typedef EditModeReturn
 * @type {array}
 * @property {boolean} 0 - whether the rendering is in "edit mode"
 * @property {update} 1 - a function that will refresh the whole {@link Widgets} component
 */

/**
 * Returns whether the current rendering is in edit mode
 * and provides a refresh function
 * @returns {EditModeReturn|Array}
 */


function useEditMode() {
  var context = useWidgetContext();
  return [context.editMode, context.queueUpdate];
}
/**
 * Returns the root design that the underlying {@link Widgets} component
 * is rendering. This can be useful as sometimes you need to access
 * global properties of the design
 * @returns {Object}
 */


function useRoot() {
  var context = useWidgetContext();
  return [context.root, context.queueUpdate];
}
/**
 * Returns the events associated with the current rendering process
 * @returns {WidgetEvents}
 */


function useEvents() {
  var context = useWidgetContext();
  return context.widgets;
}
/**
 * Provides a structure to hold the tabs.
 * ```jsx
 *     {
 *         general: {
 *             title: <FiSettings/>,
 *             content: []
 *         },
 *         "Example New Tab": {
 *             title: "Example New Tab",
 *             content: [Component1, Component2]
 *         }
 *     }
 * ```
 * @typedef Tabs
 */

/**
 * @callback AddTab
 * @param {ReactComponent} [newTab] - the component to add to the tab
 * @param {string} [title="General"] - Title of the tab to add the content to, defaults to the general tab
 * @param {string} [tab=title] - name of the tab, names are usually the same as the title, but it can be used as a different value if desired
 * @example
 //Add some tabs to the "General" page
 widgets.editor("dashboard", function () {
    const [selectedWidget] = useFocused("selectedWidget")
    const [, addTab] = useTabs(StretchInfo)
    selectedWidget && addTab(DashboardInfo)
 })
 */

/**
 * @typedef TabLayout
 * @type {array}
 * @property {Tabs} 0 - the current layout
 * @property {AddTab} 1 - a function to add a new Tab
 */

/**
 * <p>
 *     Allows one or more tabs to be added to the editor user interface. You can either use
 *     the shortcut properties to quickly add a tab, or take the return values and
 *     add more than one.
 * </p>
 * @param {ReactComponent} [newTab] - the component to add to the tab
 * @param {string} [title="General"] - Title of the tab to add the content to, defaults to the general tab
 * @param {string} [tab=title] - name of the tab, names are usually the same as the title, but it can be used as a different value if desired
 * @returns {(TabLayout|Array)}
 * @example
 //Add some tabs to the "General" page
 widgets.editor("dashboard", function () {
    const [selectedWidget] = useFocused("selectedWidget")
    const [, addTab] = useTabs(StretchInfo)
    selectedWidget && addTab(DashboardInfo)
 })
 */


function useTabs(newTab, title, tab) {
  var context = useWidgetContext();
  if (context._type !== "editor") throw new Error("Only call useTabs in an editor handler");
  var tabs = context.editor.tabs;
  var result = [tabs, function addTab(content) {
    var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "general";
    var tab = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : title;
    var tabItem = tabs[tab] = tabs[tab] || {
      title: title,
      content: []
    };
    tabItem.content.push(content);
  }];

  if (newTab) {
    result[1](newTab, title, tab);
  }

  return result;
}
/**
 * This hook is used to add items that render after the Widget's content, normally
 * on a subsequent line if you don't move them by specifying a CSS class on the
 * Widget component in the `inlineClass` property.
 *
 * @param {ReactComponent} [newContent] - optional component to add to the list
 * @returns {(ComponentListReturn|Array)}
 * @example
 * widgets.render("questions", function () {
        let widgets = useEvents()
        let types = []
        widgets.emit("questions.types", types)
        useInline(props => <AddQuestion types={types} {...props}/>)
   })
 */


function useInline(newContent) {
  var context = useWidgetContext();
  if (context._type !== "render") throw new Error("Only call useInline in a render handler");
  var inline = context.inline;
  var result = [inline, function addItem(content) {
    inline.push(content);
  }];

  if (newContent) {
    result[1](newContent);
  }

  return result;
}
/**
 * This hook is used to add items that render the context menu of a Widget.
 *
 * @param {ReactComponent} [newMenuItem] - optional component to add to the list of context items
 * @returns {(ComponentListReturn|Array)}
 * @example
 * widgets.context("dashboardItem.pie", () => {
 *    useContextMenu(ContextMenu)
 * })
 * //or
 * widgets.context("dashboardItem.pie", () => {
 *    const [menu, addMenuItem] = useContextMenu()
 *    if(menu.length < 2) addMenuItem(ExtraMenuItem)
 * })
 */


function useContextMenu(newMenuItem) {
  var _useWidgetContext2 = useWidgetContext(),
      menu = _useWidgetContext2.menu,
      _type = _useWidgetContext2._type;

  if (_type !== "context") throw new Error("Only call useContextMenu in a context handler");
  var result = [menu, function addItem(newMenuItem) {
    menu.push(newMenuItem);
  }];

  if (newMenuItem) {
    result[1](newMenuItem);
  }

  return result;
}
/**
 * This hook is used to add items that render the content of a Widget.
 * It is the primary function of the widget to display this content
 * outside of edit mode
 *
 * @param {ReactComponent} [newContent] - optional component to add to the list
 * @returns {(ComponentListReturn|Array)}
 * @example
 * widgets.render("dashboardItem.pie", () => {
 *    useContent(Render)
 * })
 * //or
 * widgets.render("dashboardItem.pie", () => {
 *    const [existingContents, addContent] = useContent()
 *    if(existingContents.length < 2) addContent(Render)
 * })
 */


function useContent(newContent) {
  var context = useWidgetContext();
  if (context._type !== "render") throw new Error("Only call useContent in a render handler");
  var content = context.content;
  var result = [content, function addItem(newContent) {
    content.push(newContent);
  }];

  if (newContent) {
    result[1](newContent);
  }

  return result;
}

function mergeArrays(objValue, srcValue) {
  if (Array.isArray(objValue)) {
    return objValue.concat(Array.isArray(srcValue) ? srcValue : [srcValue]);
  }
}
/**
 * This hook is used to add items to the "layout canvas" of a Widgets
 * component.  You must use this only when responding to
 * a [configure]{@link Events#configure} event to place items in the content area or one of
 * the other slots in the canvas.
 * @param {Layout} [layoutToMerge] - optional new layout to merge in
 * @returns {(LayoutReturn|Array)}
 * @example
 widgets.configure("survey", function () {
        const [design] = useDesign()
        if (design.questions) {
            useLayout({right: ReadOnly})
        }
     })
 */


function useLayout(layoutToMerge) {
  var context = useWidgetContext();
  if (context._type !== "configure") throw new Error("Only call useLayout in a configure handler");
  var layout = context.layout;
  var result = [layout, function setLayout() {
    var newLayout = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    (0, _mergeWith.default)(layout, newLayout, mergeArrays);
  }];

  if (layoutToMerge) {
    result[1](layoutToMerge);
  }

  return result;
}
/**
 * This hook is used to add items to the an area to the right of
 * a Widget's contents.  Normally used for edit mode components like
 * trashing an item or moving it. It can only be used in a `render` event.
 *
 * You can move the location of the endOfLine items by passing a
 * CSS class to style them differently.
 *
 * @param {ReactComponent} [newContent] - optional component to add to the list
 * @returns {(ComponentListReturn|Array)}
 * @example
 widgets.render("copyable", function () {
         useEndOfLine(Render)
     })
 */


function useEndOfLine(newContent) {
  var context = useWidgetContext();
  var endOfLine = context.endOfLine;
  var result = [endOfLine, function addItem(content) {
    endOfLine.push(content);
  }];

  if (newContent) {
    result[1](newContent);
  }
}