"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Widgets = exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactJss = _interopRequireDefault(require("react-jss"));

var _typeExtractor = _interopRequireDefault(require("./type-extractor"));

var _reactstrap = require("reactstrap");

require("bootstrap/dist/css/bootstrap.min.css");

var _widgetRow = _interopRequireDefault(require("./widget-row"));

var _shortid = _interopRequireDefault(require("shortid"));

var _fi = require("react-icons/fi");

var _contexts = require("./contexts");

var _swallowClicks = _interopRequireDefault(require("./swallow-clicks"));

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var _widgetStyles = require("./widget-styles");

var _utilities = require("./utilities");

var _events = require("./events");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _eventemitter = require("eventemitter2");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Used to indicate that the Widgets content or design has changed
 * @callback OnUpdated
 */
var ContextMenu = (0, _reactJss.default)(_widgetStyles.styles)(function ContextMenu(_ref) {
  var classes = _ref.classes;
  var context = (0, _contexts.useWidgetContext)();
  var id = context.id,
      widgets = context.widgets,
      contextMenu = context.contextMenu,
      documentType = context.documentType;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      contextOpen = _useState2[0],
      setContextOpen = _useState2[1];

  (0, _contexts.useFocus)();
  var menu = [];

  _contexts.useWidgetContext.with(_objectSpread({}, context, {
    _type: "context",
    menu: menu
  }), function () {
    widgets.emit("context.".concat(documentType), menu);
  });

  if (menu.length === 0 && contextOpen) {
    setContextOpen(false);
  }

  return _react.default.createElement(_utilities.Wrapper, null, _react.default.createElement(_swallowClicks.default, {
    style: {
      display: menu.length > 0 ? "initial" : "none"
    }
  }, _react.default.createElement("div", {
    "data-testid": "contextopen",
    className: classes.contextButton,
    id: "context".concat(id),
    onClick: function onClick() {
      return setContextOpen(!contextOpen);
    }
  }, contextMenu || _react.default.createElement(_fi.FiMenu, null)), _react.default.createElement(_reactstrap.Popover, {
    placement: "left",
    isOpen: contextOpen,
    target: "context".concat(id),
    toggle: function toggle() {
      return setContextOpen(!contextOpen);
    }
  }, _react.default.createElement(_reactstrap.PopoverBody, null, menu.map(function (Item, index) {
    return _react.default.createElement(Item, {
      key: index
    });
  })))));
});
ContextMenu.propTypes = {
  layout: _propTypes.default.shape({
    headerLeft: _propTypes.default.arrayOf(_propTypes.default.any),
    headerCentre: _propTypes.default.arrayOf(_propTypes.default.any),
    headerRight: _propTypes.default.arrayOf(_propTypes.default.any),
    left: _propTypes.default.arrayOf(_propTypes.default.any),
    content: _propTypes.default.arrayOf(_propTypes.default.any),
    right: _propTypes.default.arrayOf(_propTypes.default.any),
    footer: _propTypes.default.arrayOf(_propTypes.default.any)
  }),
  className: _propTypes.default.any,
  id: _propTypes.default.any,
  onClick: _propTypes.default.func,
  contextMenu: _propTypes.default.any,
  open: _propTypes.default.any,
  contents: _propTypes.default.func
  /**
   * Widgets must always have a `design` specified and usually a `document` if there is data capture from the user.
   * If you want to enable automatic editing then you put an [EditorFrame]{@link EditorFrame} component
   * inside.  You can also put an [Editor]{@link Editor} anywhere and bind them together using ids and a
   * shared `bag`.
   * @classdesc The main canvas for documents.  You add a Widgets component to your application as the root of
   * displaying a document and/or editing its design.
   * @kind component
   * @property {Object} design - the design of the document that will be edited by the Widgets
   * @property {Object} [document] - the document contents being edited
   * @property {string} [type] - the type of the document being edited, if this isn't specified the system tries to
   * extract it from the design or the document itself and provides an event for plugins to override or provide mapping
   * @property {boolean} [editable] - indicates whether the component can enter "edit mode"
   * @property {OnUpdated} [onUpdated] - called when the document has changed
   * @property {WidgetEvents} [widgets] - an event source to use, useful for testing, defaults to the globalWidgets event source
   * @property {Object} [style] - a style to apply to the outer div rendering the widgets
   * @property {string} [className] - a CSS class name for the outer div
   * @property {string} [leftClass] - CSS class name for the left panel
   * @property {string} [rightClass] - CSS class name for the right panel
   * @property {string} [headerLeftClass] - CSS class name for the left of the header
   * @property {string} [headerRightClass] - CSS class name for the right of the header
   * @property {string} [headerCentreClass] - CSS class name for the main header area
   * @property {string} [footerClass] - CSS class name for the footer
   * @property {string} [centreClass] - CSS class name for the main Widgets content area
   * @property {Object} [bag] - an object that is used to share data between editors and the display component, you only set
   * this when you are using an external {@link Editor} where it should share the same object
   * @property {string} [id] - an identifier for this display component, which should be matched with a similar id on the
   * {@link Editor} component if using an external editor
   * @example
   * return <Widgets design={someDesign}>
   *     <EditorFrame/>
   * </Widgets>
   * @see {@link EditorFrame} and {@link Editor} for WYSIWYG design interfaces
   */

};
var Widgets = (0, _reactJss.default)(_widgetStyles.styles)(function Widgets(_ref2) {
  var _ref2$widgets = _ref2.widgets,
      widgets = _ref2$widgets === void 0 ? _events.globalWidgets : _ref2$widgets,
      localId = _ref2.id,
      _ref2$bag = _ref2.bag,
      localBag = _ref2$bag === void 0 ? {} : _ref2$bag,
      children = _ref2.children,
      contextMenu = _ref2.contextMenu,
      className = _ref2.className,
      footerClass = _ref2.footerClass,
      headerClass = _ref2.headerClass,
      leftClass = _ref2.leftClass,
      rightClass = _ref2.rightClass,
      contentClass = _ref2.contentClass,
      classes = _ref2.classes,
      style = _ref2.style,
      document = _ref2.document,
      design = _ref2.design,
      type = _ref2.type,
      editable = _ref2.editable,
      _ref2$onUpdated = _ref2.onUpdated,
      onUpdated = _ref2$onUpdated === void 0 ? _utilities.noop : _ref2$onUpdated,
      props = _objectWithoutProperties(_ref2, ["widgets", "id", "bag", "children", "contextMenu", "className", "footerClass", "headerClass", "leftClass", "rightClass", "contentClass", "classes", "style", "document", "design", "type", "editable", "onUpdated"]);

  if (!document || !design) {
    return _react.default.createElement("div", null, "You must set 'document' and 'design' parameters");
  }

  var documentType = (0, _typeExtractor.default)({
    type: type
  }, document, design);

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      editMode = _useState4[0],
      setEditMode = _useState4[1];

  var _useState5 = (0, _react.useState)((0, _utilities.calculateBreakpoint)(window.width)),
      _useState6 = _slicedToArray(_useState5, 2),
      size = _useState6[0],
      setSize = _useState6[1];

  if (editMode && !editable) setEditMode(false);

  var _useState7 = (0, _react.useState)(localBag),
      _useState8 = _slicedToArray(_useState7, 1),
      myBag = _useState8[0];

  var _useState9 = (0, _react.useState)(localId || _shortid.default.generate()),
      _useState10 = _slicedToArray(_useState9, 1),
      id = _useState10[0];

  var _useState11 = (0, _react.useState)("update"),
      _useState12 = _slicedToArray(_useState11, 2),
      doReactUpdate = _useState12[1];

  var _useState13 = (0, _react.useState)(0),
      _useState14 = _slicedToArray(_useState13, 2),
      height = _useState14[0],
      setHeight = _useState14[1];

  var focus = myBag.focus = myBag.focus || {};

  function setFocus(newFocus) {
    if (!(0, _isEqual.default)(focus, newFocus)) {
      focus = myBag.focus = newFocus;
      widgets.emit("focus.".concat(documentType, ".").concat(id), newFocus);
    }
  }

  function onChanged() {
    onUpdated({
      document: document,
      design: design
    });
  } //Handle updates to the document


  (0, _react.useEffect)(function () {
    widgets.on("updatevalue.".concat(documentType, ".").concat(id, ".*"), onChanged);
    return function cleanup() {
      widgets.removeListener("updatevalue.".concat(documentType, ".").concat(id, ".*"), onChanged);
    };
  });
  var queueUpdate = (0, _debounce.default)(doReactUpdate, 150, {
    maxWait: 1000,
    leading: false,
    trailing: true
  });
  var ref = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    var newHeight = ref.current.offsetHeight;
    if (newHeight !== height) setHeight(newHeight);
    var newSize = (0, _utilities.calculateBreakpoint)(ref.current.offsetWidth);
    if (newSize !== size) setSize(newSize);
    window.addEventListener('resize', queueUpdate);
    return function () {
      window.removeEventListener('resize', queueUpdate);
    };
  });
  var layout = {
    headerLeft: [],
    headerCentre: [],
    headerRight: [],
    left: [],
    content: [],
    right: [],
    footer: [],
    context: []
  };

  var _useWidgetContext = (0, _contexts.useWidgetContext)(),
      _useWidgetContext$roo = _useWidgetContext.root,
      root = _useWidgetContext$roo === void 0 ? design : _useWidgetContext$roo,
      _useWidgetContext$bag = _useWidgetContext.bag,
      bag = _useWidgetContext$bag === void 0 ? myBag : _useWidgetContext$bag;

  var context = _objectSpread({}, props, {
    id: id,
    size: size,
    documentType: documentType,
    root: root,
    design: design,
    document: document,
    layout: layout,
    focus: focus,
    setFocus: setFocus,
    onChanged: onChanged,
    queueUpdate: queueUpdate,
    bag: bag,
    editMode: editMode,
    setEditMode: setEditMode,
    widgets: widgets,
    height: height
  });

  _contexts.useWidgetContext.with(_objectSpread({}, context, {
    _type: "configure"
  }), function () {
    (0, _contexts.useDesign)(design);
    widgets.emit("configure.".concat(documentType), context);
  });

  return _react.default.createElement(_contexts.WidgetContext.Provider, {
    value: context
  }, _react.default.createElement("div", {
    className: classes.holder,
    style: style,
    ref: ref
  }, _react.default.createElement(_reactstrap.Container, {
    onClick: function onClick() {
      return setFocus(design);
    },
    fluid: true,
    className: classes.widgets
  },
  /* The Header */
  (0, _widgetRow.default)(_objectSpread({
    colType: "xs",
    className: headerClass || classes.header,
    left: layout.headerLeft,
    right: layout.headerRight,
    centre: layout.headerCentre
  }, context)),
  /* The Centre */
  (0, _widgetRow.default)(_objectSpread({
    style: {
      paddingBottom: editMode ? "50vh" : 0
    },
    className: className || classes.body,
    leftClass: leftClass || classes.left,
    rightClass: rightClass || classes.right,
    centreClass: contentClass || classes.content,
    left: layout.left,
    right: layout.right,
    centre: layout.content
  }, context)),
  /* The Footer */
  (0, _widgetRow.default)(_objectSpread({
    className: footerClass || classes.footer,
    centre: layout.footer
  }, context)), _react.default.createElement(ContextMenu, null), children)));
});
exports.Widgets = Widgets;
Widgets.propTypes = {
  design: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]).isRequired,
  document: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  widgets: _propTypes.default.instanceOf(_eventemitter.EventEmitter2),
  bag: _propTypes.default.object,
  editable: _propTypes.default.bool,
  style: _propTypes.default.object,
  className: _propTypes.default.string,
  leftClass: _propTypes.default.string,
  rightClass: _propTypes.default.string,
  headerClass: _propTypes.default.string,
  footerClass: _propTypes.default.string,
  centreClass: _propTypes.default.string,
  onUpdated: _propTypes.default.func,
  type: _propTypes.default.string
};
var _default = Widgets;
exports.default = _default;