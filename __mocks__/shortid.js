let id = 0
module.exports = {
    generate() {
        return id++
    },
    reset() {
        id = 0
    }
}
