# React Inversion Of Control Widgets

The widgets library is a series of components that provide a canvas for designing and then editing or viewing documents.  The documents are expressed as Javascript objects, serializable to JSON.

The principle is to have a WYSIWYG editor for your own documents, to enable highly customised systems.  You might use it to create a dynamic form filling interface where you can allow your users to design a series of forms or perhaps a dashboard designer and catalogue.

The library contains the bare minimum of components and uses a loosely coupled approach to editors, meaning that you can write code to use the canvas the way you see fit, and you can augment the experience later without changing any base code, plus you can reuse useful editors across many types of document.

![Image](https://s3-eu-west-1.amazonaws.com/shared-localstorage/demo.gif)

In the example you see above, most of the look and funcitonality has been developed in the examples, it's all very simple.  The *Widgets* component is just providing some places to put content and triggering events so you can loosely accrete functionality as you go along.

### Resources

You can read the {@tutorial Getting Started} guide, or dive into the {@tutorial Core Concepts} to get a feeling for how Inversion of Control works. {@tutorial Hooks and Events} looks into how Inversion of Control is implemented in this library.

### Project Structure of source project

The project examples are all in the `src/` folder. The `index.js` and `App.js` files make an example React app that shows off all of the features and the code for the editors etc can be found in the `src/examples/` folder.

`src/component` contains the code for the actual component, but it is exposed through the `index.js` in the project root.

You can find some example JSON that you could write to localStorage to get the examples bootstrapped in the `src/examples/example` folder.

### Examples in the source project

`npm start` to start the example app on localhost:3009
