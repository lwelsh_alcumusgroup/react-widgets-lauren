const events = {
    configure: {
        survey: {
            type: 'survey',
            size: 'md',
            design: {
                questions: []
            },
            layout: {
                right: []
            },
            bag: {}
        },
        wildcard: {
            design: {
                title: ''
            },
            layout: {
                headerCentre: []
            },
            editor: {
                tabs: {
                    general: {
                        content: []
                    }
                }
            }
        }
    },
    editor: {
        survey: {
            editor: {
                tabs: {
                    general: {
                        content: []
                    }
                }
            }
        },
        wildcard: {
            design: {
                title: ''
            },
            layout: {
                headerCentre: []
            },
            editor: {
                tabs: {
                    general: {
                        content: []
                    }
                }
            }
        }
    },
    questions: {
        types: []
    },
    question: {
        text: {
            content: []
        }
    }
};

export { events };