import React from 'react';
import renderer from 'react-test-renderer';
import {globalWidgets} from '../src/component/events';

export const widgets = globalWidgets;

export const renderComponent = (Component, props = {}) => {
    return renderer.create(<Component {...props} /> ).toJSON();
};

export const mockContext = (value = {}, setValue = () => jest.fn()) => {
    return () => ([value, setValue]);
};

