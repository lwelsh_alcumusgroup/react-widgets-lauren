If you’ve had a look through {@tutorial Core Concepts} you’ll understand that React IoC Widgets creates a user interface to display and design documents by using an Inversion of Control, where loaded modules have the opportunity to add the user interfaces applicable for the document and it’s internal elements.

After reading this you should understand how React IoC Widgets implements the control inversion and how you can access the various parts of the generic framework when you are implementing modules.

### Underlying Principle

React IoC Widgets uses an event emitter {@link WidgetEvents} based off [EventEmitter2][1] to provide a loosely coupled way of having modules provide their user interfaces.

When it’s time to render something, {@link Widgets} and {@link Widget} fire events and the handler of those events populate either the display or editor canvas with standard React Components.

To maintain high performance and minimise unnecessary redraws React IoC Widgets provides access to all framework features using [hooks][2]. Hooks can easily trigger a redraw in just the parts of the interface that need updating when something changes and are the ideal way to link editors and content together.

### {@link Widgets} events

{@link Widgets} represents the whole document. Documents have a single “type” that is extracted from the *design* or specified on the actual {@link Widgets} component. Modules can therefore choose to respond to only certain document types.

{@link WidgetEvents} uses wildcards, and all {@link Widgets} events are namespaced with the document type, so a module can choose to only work for certain document types or it can use a “\*” in that position to indicate it can consider all document types.

```jsx
//Only handle one type of document
globalWidgets.on("configure.someTypeOfDocument", setup)

//Handle all types of document
globalWidgets.on("configure.*", setup)
```
Responding to an event doesn’t do much, the handler has to choose to add things to the layout or editor canvas - it can do this conditionally of course - this allows the module to decide what device it is running on, or to further inspect the document or design before deciding to add its interfaces.

```jsx
globalWidgets.on("configure.*", function() {
    const [design] = useDesign()
    if(design.someHint && window.width > 500) useLayout({content:  [MyUserInterface]})
})
```

Here you can see the first taste of Hooks, retrieving the current design and providing an interface to add things to the display layout.

To make setting up events easier, {@link WidgetEvents} has special methods to add handlers for the standard events. For {@link Widgets} these are `configure(type, handler)`, `editor(type, handler)` and `context(type, handler)`. If you would have passed “\*” for `type` you can omit it.

* `configure` is used to setup all core components, you’d normally add any layout for display components in this event.  It’s fired when the document is first displayed and if there is a core change to the design. Layout is added using the {@link useLayout} hook.
* `context` is used to add components to the context menu, it’s fired every time the focus changes inside the {@link Widgets}. Often it will make a decision to add menu items based on what the current focus is using the {@link useFocused} hook. Items are added using the {@link useContextMenu} hook.
* `editor` is used to add editor tabs to the editor interface.  It’s fired when the focus changes.  It’s fired even when the component isn’t in edit mode, because the editor activation button is only shown if something added a UI to the editor tabs.  Like `context` it often uses {@link useFocused} to decide if information should be displayed.  Tabs are added using the {@link useTabs} hook.

```jsx
globalWidgets.configure("someDocumentType", function() {
    useLayout({content:  [MyUserInterface]})
})

//Add an editor for certain elements only
globalWidgets.editor(function() {
    const [question] = useFocused("selectedQuestion")
    if(question && question.type == "list") useTabs(EditPossibleAnswers, "Answers")
})
```

### Your UI Components

You’ll want to put components into the layout, content menu or editor to create your user experience.  These components are going to need access to the design, the document and potentially other data in order to render the right information.

Basically the `document` is supposed to represent any user data that has been captured.  There might not be much of that in a “dashboard” app, and there will be tons of it in a “survey” app - but there’s always going to be a document.  The `design` is where the layout happens and this is going to exist for all document types.  Finally there is a `bag` - a place to share data and hold on to non-persistent UI state.  For instance the only standard thing in the `bag` is a member called `focus` that contains the currently focused item - though there are better ways of getting at it!

In your component, you get access to the `document` with the {@link useDocument} hook, the `design` with {@link useDesign} and the `bag` with {@link useBag}.

Any additional properties set on the {@link Widgets} component (or set further up the tree when we come on to look at {@link Widget} components later), can also be accessed using {@link useWidgetContext}.

```jsx
function MyUserInterface() {
    const [design] = useDesign()
    const [document, update] = useDocument()
    const {specialProperty} = useWidgetContext()
    return <div>
        <h1 style={specialProperty}>{design.title}</h1>
        <label for="content">Your views</label>
        <input name="content" value={document.answer || ""} onChange={event=>update({answer: event.target.value})}/>
    </div>
}
```

### Crafting Modules

You’ll want to put your components and handlers in modules and you’ll want to write tests for them.  While React IoC Widgets provides a {@link globalWidgets} event emitter, unit tests benefit from having the modules able to isolate the emitter they connect with.

You could use `globalWidgets.removeAllListeners()` between each test or you can write modules that export a function that takes a {@link WidgetEvents} parameter and then add their handlers to that.  This is the preferred approach and allows for more fine grained control.

A module might look like this:

```jsx
import React from "react"
import {useDesign, useLayout} from "react-ioc-widgets"

function MyComponent() {
    let [design] = useDesign()
    return <h1>{design.title}</h1>
}

export default function(widgets) {
   widgets.configure("someType", function() {
          useLayout({content: [MyComponent]})
    })
}
```

You could then dynamically load modules or import them.  A standard importer would use {@link globalWidgets}, but your tests could use localised versions.

```jsx
function App() {
   let hasComponents = useAsync(async () => {
        try {
            await Promise.all(components.map(async item => {
                let module = await import(`./examples/${item}`)
                module.default(globalWidgets)
            }))
        } catch(e) {
            console.error(e.stack)
        }
        return true
    })
    return !hasComponents ? <div>Wait...</div> : <Widgets design={design} document={document} type="someType"/>
}
```

Here the modules are loaded and initialised with {@link globalWidgets} inside the app, but you are free
import them in tests and pass a different value.

### Inverting Control Yourself

Your components added to the layout or editor can choose to create their own points for control inversion - the {@tutorial Core Concepts} tutorial covers an example of where this can be very powerful.

To invert control yourself you will render {@link Widget} components inside your own components.

A {@link Widget} component can act a reference frame and you are free to put children inside it, these will participate alongside any user interface added by a module.  Often though, for your innermost components, you’ll choose to just implement all of the interface in a module. <small><em>Of course your module might continue the theme and invert control itself!</em></small>

{@link WidgetEvents} only has one event for rendering a {@link Widget} `render` (and it has a helper for it) but this has access to three areas for user interface - two for the editor and the standard content.  You access them with hooks. {@link useContent} is the display content, {@link useEndOfLine} places content horizontally after the normal content in edit mode only and {@link useInline} places content logically after (normally vertically) the normal display content.  These editor regions are very useful for buttons that affect the item - like moving or deleting it.   Normally you use `Inline` for buttons to add new elements to lists etc.

```jsx
globalWidgets.render("question.text", function() {
     useContent(TextQuestion)
})
globalWidgets.render("listItem", function() {
     useEndOfLine(MoveListItemUI)
})
```

The editors mean you will often use a {@link Widget} to represent the group which contains items (the array of questions in the example).  This is so you can add a button to create new questions or sort the list etc.

```jsx
function Questions({questions, widgets = useWidgets, fallbackCaption, editMode, answers, ...props}) {
    let update
    ([questions, update] = useDesign(questions))
    //Outer widget will probably only have edit mode content added, we are rendering its contents
    return <Widget {...props}
                   isSelected={focus => focus.selectedQuestions === questions}
                   focusOn={{selectedQuestions: questions}}
                   design={questions}
                   document={answers}
                   type="questions">
         {
            questions.length
                ? questions.map((question, index) => (
                    //Inner widget is going to be rendered by a module
                    <Widget {...props}
                                    key={question.id || index}
                                    design={question}
                                    document={answers}
                                    value={answers[question.name]}
                                    setValue={(value) => {
                                        answers[question.name] = value
                                    }}
                                    focusOn={{
                                        selectedQuestions: questions,
                                        selectedQuestion: question,
                                        answers
                                    }}
                                    type={[`question.${question.type || 'default'}`, "list", "copyable"]}/>))
                : (
                    <div>
                        {fallbackCaption !== undefined ? fallbackCaption : "No questions..."}
                    </div>)
        }
    </Widget>
} 
```

Many times your {@link Widget} components will eventually represent a primitive value, or a value you want to set atomically.  You can decorate the {@link Widget} with `value` and `setValue` properties to extract the information from the document and update it, then use the {@link useValue} hook in the component that edits the widget to update it. Like this:

```jsx
const Render = injectSheet(style)(({classes}) => {
    const [value, setValue] = useValue()
    const [design] = useDesign()
    return (
        <FormGroup key={design.id}>
                <Label>{design.question}</label>
                <Input type="text" value={value || ""}
                       onChange={setValue} placeholder={design.placeholder || ""}/>
        </FormGroup>
    )
})

```

### A Word On Focus

Quite a lot of the editor functions rely on the principle of *focus*.  In a {@link Widgets} component some *design* or special focus object is always "in focus".  When the user focuses a component or clicks on it, the {@link Widgets} component will set the current focus to the *design* of the containing {@link Widget} or if there isn't one then it's going to set it to the {@link Widgets} `design`.  Sometimes its helpful to have something custom in focus - the example above did that so that both the list of questions and the selected question were "in focus" when a list item is clicked. You achieve this using {@link Widget}'s `focusOn` property.  You can also override when an item knows if it is selected in focus - this is important in the above example as the list of questions needs to know it's still focused, it does that by having an `isSelected` property set to a function which gets a copy of the currently focused item and returns what that means for the {@link Widget}'s current focus state.

If no `isSelected` property is present an item is focused if the current `focus` is set to it's `focusOn` property or `design` if there is no specific focus object.

![widgets.013.jpeg][image-2]

### Conclusion

The {@tutorial Getting Started} tutorial has a worked example of building a simple inverted interface.

[1]:	https://github.com/EventEmitter2/EventEmitter2
[2]:	https://reactjs.org/docs/hooks-overview.html


[image-2]:	https://s3-eu-west-1.amazonaws.com/shared-localstorage/widgets.013.jpeg
