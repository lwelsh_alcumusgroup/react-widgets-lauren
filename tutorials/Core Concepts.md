React IoC Widgets is a generic framework for creating many types of document, the way the document is represented and displayed to the user depends on what modules are loaded.  The framework is based on the principle of [Inversion of Control][1] which is a subject you can do additional research on if you feel the need.

In this package, the top level {@link Widgets} component is very light weight, it merely offers up the ability to render a document to any modules which might have been loaded.  The loaded modules are offered an opportunity to place their user interface in a series of well known locations on the Widgets Layout canvas.

![widgets.012.jpeg][image-1]

The User Interface created by the modules can also benefit from providing additional hook points so that the pattern may continue with the developer being able to make a very dynamic system that can be augmented over time without changing the core code.

One of the key tricks to working with this framework is deciding how to divide your application’s interface into additional sub components.  You are choosing the moment to move from an “inverted” model of components to the traditional imperative one to implement the final concrete parts of the interface.

### Example

Let’s imagine we want to create an application to capture data from the user, this is an ideal case for Inversion of Control.  The {@tutorial Getting Started} tutorial will work through this as code, here we’ll deal with the concepts behind it.

When working with this library, the first thing is to create an interface for the core document type.  In this case let’s say you call this type ‘survey’.  You’ll write a component that is invoked when the ‘survey’ document type is identified.  Now this component could just choose to create its whole interface imperatively and push it into a part of the layout.  However, if you do that, you’ll lack the ability to enhance how the user interacts with individual survey questions in the future without changing the core code base.

To create a cool survey interface it would be nice if we, or other developers with minimal knowledge of the document structure, could create innovative new ways of displaying questions and capturing the user input.  That way we could enhance the system over time or even provide packs of premium components or client specific ways of entering information.  If we design our system right we can do this without ever touching any of the core code.

So we might now consider our Survey has a list of “questions”, so we will create an Inversion Point for each question.  We would achieve this using the {@link Widget} component (or one of its other flavours {@link SortableWidget} and {@link SortableWidgetContainer}).

Our initial user interface will now create one {@link Widget} for each *question*, providing it with just the configuration information for that *question* and a way of getting and setting the current answer in addition to a *type* which is used to trigger the loaded modules to render an interface to show the question.

We can provide a fall back interface that gives the user a text box to enter the answer, irrespective of the *type* and then any other loaded module can choose to replace that interface with its own should it be more suitable.

By placing more hints in the design of the document our modules can be smarter about choosing when to override the default behaviour.

You can imagine a list of such items being considered for a question with an answer that is the current temperature in celsius with a minimum of -50c and a maximum of 70c.

* Text box to enter the data (fallback)
* Numeric input with minimum and maximum values
* Range slider
* Custom temperature entry input ([Mobiscroll][2] have a great one) for mobile devices

In addition we could add extra “quick set buttons” to any of the above:

* if we detected the ability to sense temperature, we could add a button to get the current temperature
* a button to use a web service to get the current temperature at our geographic location
* a smarter button to use the web service to get the minimum or maximum historical temperature at our geographic location

Obviously we can only make choices about which components to use given enough hints about the type of data being captured and the capabilities of the device on which the interface is running.  Due to this it’s vital we make our document *designs* rich enough to provide the interface with enough information.

As we go down this track, it’s going to be increasingly important to provide our users with an editing experience for documents so that they can set all of these properties and hints.  Fortunately using {@link EditorFrame} or {@link Editor} it’s very easy to also create an Inversion of Control editing interface for each document type.

### Editing

 The framework also provides an editing canvas which the modules can place their editing user interfaces inside. This editor canvas will be displayed to the user if the outer component is editable. The user has a button which theycan activate to start the editing experience and then work with the provided elements to setup and modify the document’s design.

![widgets.013.jpeg][image-2]

The editing interface can be shipped alongside components in the same file or in completely separate modules.

### Multiple Components For A Single Type

We touched on the ability to have multiple editors for a type above with our celsius input having both an editor box and short cut buttons, this can be very handy.  In the editor it’s even more useful.

There will probably be a lot of lists in the design, questions, optional values for an answer etc etc.  Imagine that we had a “list” type that identified this and enabled us to provide a single user interface to work with any list - maybe duplicating items, moving them around and deleting them.

To facilitate this, the {@link Widget} components in this framework can take an array of types, each type will be offered up and modules can add their user interfaces.  So a `type={["question.text", "list", "listCopyable", "listCanDelete"]}` question might show an interface from one module to edit the question, a couple of buttons from another one to reorganise the list, another module might provide the features of duplicating list items and deleting them.  Furthermore at some time in the future any of those interfaces could be superseded by a new module that had a cleverer way of achieving them, perhaps only on a specific kind of device.

### Priority

Clearly if lots of things all can render interfaces for the same items there needs to be a way of ordering them, both for the purposes of display if multiple are to be active at the same time, but also so that higher priority items can remove the  fallback interfaces.

When you work with React IoC Widgets you will be putting your own React Components into the various layout slots, you are able to add a couple of properties to the component to give it a priority and to have a function called on it before rendering that is provided with the current list of components - which it is free to modify.

```jsx
function MyFancyUI() {
    return <div>Not really that clever</div>
}
MyFancyUI.priority = 10 //default is 1, higher is better

//Can process and update the list, or just return a new array
MyFancyUI.onBeforeRender(function (list) {
    //Only render me - however a higher priority 
    //component can just do the same thing and 
    //remove me
    return  [MyFancyUI]  
})
```

### Conclusion

Hopefully this has given you an overview of how we can consider Inversion of Control using this library, the {@tutorial Getting Started} guide will look at code to achieve this and the {@tutorial Hooks and Events} will dive into the concepts behind how React IoC Widgets implements its loosely coupled model for inverting control.

[1]:	https://en.wikipedia.org/wiki/Inversion_of_control
[2]:	https://demo.mobiscroll.com/jquery/measurement/temperature

[image-1]:	https://s3-eu-west-1.amazonaws.com/shared-localstorage/widgets.012.jpeg
[image-2]:	https://s3-eu-west-1.amazonaws.com/shared-localstorage/widgets.013.jpeg
